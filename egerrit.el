;;; egerrit.el --- An interface to Gerrit -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2022 Niklas Eklund

;; Author: Niklas Eklund <niklas.eklund@posteo.net>
;; URL: https://www.gitlab.com/niklaseklund/egerrit
;; Version: 0.2
;; Package-Requires: ((emacs "27.1"))
;; Keywords: convenience tools

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package allows Emacs to interface Gerrit.  It provides
;; functionality to interact with changes, such as adding reviewers and
;; sending messages.  It provides access to jobs and review comments
;; for different patch sets.  Both the job outputs and review comments
;; utilizes compilation-mode which makes it effortless to jump to the
;; source code.

;;;;; External dependencies

;; + curl
;; + diff
;; + git

;;; Code:

;;;; Requirements

(require 'ansi-color)
(require 'auth-source)
(require 'cl-lib)
(require 'comint)
(require 'diff-mode)
(require 'parse-time)
(require 'subr-x)
(require 'text-property-search)
(require 'timer)

(declare-function json-parse-buffer "json.c" (&rest args))

;;;;; Custom

(defcustom egerrit-dashboard-config
  `((:name "Subject" :function egerrit--subject-str :length 60)
    (:name "Status" :function egerrit--change-status-str :length 10)
    (:name "Project" :function egerrit--project-str :length 20)
    (:name "Owner" :function egerrit--owner-str :length 30)
    (:name "Assignee" :function egerrit--assignee-str :length 30)
    (:name "CR" :function egerrit--code-review-str :length 3)
    (:name "SR" :function egerrit--secondary-review-str :length 3)
    (:name "V" :function egerrit--verified-review-str :length 3)
    (:name "Topic" :function egerrit--topic-str :length 20)
    (:name "Updated" :function egerrit--updated-str :length 30))
  "This alist describes the dashboard layout.
It describes what
to show and which functions to call to get the right data."
  :type '(repeat symbol)
  :group 'egerrit)

(defcustom egerrit-revision-config
  `((:name "Revision" :function egerrit--revision-number-str :length 10)
    (:name "Uploader" :function egerrit--owner-str :length 30)
    (:name "Comment authors" :function egerrit--revision-comment-authors-str :length 50)
    (:name "Comments unresolved" :function egerrit--revision-unresolved-str :length 20)
    (:name "Jobs" :function egerrit--revision-jobs-str :length 10)
    (:name "Created" :function egerrit--updated-str :length 15))
  "This alist describes the revision layout.
It describes what
to show and which functions to call to get the right data."
  :type '(repeat symbol)
  :group 'egerrit)

(defcustom egerrit-job-config
  `((:name "Name" :function egerrit--job-name :length 70)
    (:name "Status" :function egerrit--job-status-str :length 30)
    (:name "Time" :function egerrit--duration-str :length 30)
    (:name "Date" :function egerrit--updated-str :length 30))
  "This alist describes the job layout.
It describes what
to show and which functions to call to get the right data."
  :type '(repeat symbol)
  :group 'egerrit)

(defcustom egerrit-request-url "www.gerrit.com"
  "URL to the Gerrit server."
  :type 'string
  :group 'egerrit)

(defcustom egerrit-changes-queries
  `(("Gerrit Dashboard" . (("Has draft comments" . ("has:draft"))
                           ("Your turn" . ("attention:self"))
                           ("Work in progress" . ("is:open owner:self is:wip"))
                           ("Outgoing reviews" . ("is:open owner:self -is:wip -is:ignored"))
                           ("Incoming reviews" . ("is:open -owner:self -is:wip -is:ignored (reviewer:self OR assignee:self)"))
                           ("CCed on" . ("is:open -is:ignored cc:self"))
                           ("Recently closed" . ("is:closed -is:ignored (-is:wip OR owner:self) (owner:self OR reviewer:self OR assignee:self OR cc:self)" 20))))
    ("URL" . egerrit-query-from-url)
    ("Custom" . egerrit-custom-query))
  "Queries to use for changes."
  :type 'alist
  :group 'egerrit)

(defcustom egerrit-max-changes 500
  "Max number of requested changes."
  :type 'int
  :group 'egerrit)

(defcustom egerrit-job-regexp nil
  "Function that return a regexp to capture jobs."
  :type 'function
  :group 'egerrit)

(defcustom egerrit-project-roots nil
  "Variable to store project roots."
  :type 'alist
  :group 'egerrit)

(defcustom egerrit-comment-block-list nil
  "A list of regexp to match comments that should be blocked."
  :type 'list
  :group 'egerrit)

(defcustom egerrit-commit-message-source-code-function #'ignore
  "A function that takes no arguments and displays a commit message."
  :type 'function
  :group 'egerrit)

(defcustom egerrit-job-function #'egerrit--add-change-jobs
  "A function that takes a change and updates it with CI jobs."
  :type 'function
  :group 'egerrit)

(defcustom egerrit-comment-formatter #'ignore
  "A function that takes no argument."
  :type 'function
  :group 'egerrit)

(defcustom egerrit-status-formatter #'ignore
  "A function that takes no argument."
  :type 'function
  :group 'egerrit)

(defcustom egerrit-change-wip-status "wip"
  "A string to be shown when status is WIP."
  :type 'string
  :group 'egerrit)

(defcustom egerrit-change-comments-status "comments"
  "A string to be shown when status is comments."
  :type 'string
  :group 'egerrit)

(defcustom egerrit-git-diff-context 3
  "A variable which determines lines of context with git diff."
  :type 'int
  :group 'egerrit)

(defcustom egerrit-gerrit-conversation-context 5
  "A variable which determines lines of context in conversations."
  :type 'int
  :group 'egerrit)

(defcustom egerrit-user-name nil
  "A variable to hold the user-name used on Gerrit."
  :type 'string
  :group 'egerrit)

;;;;; Variables

(defvar egerrit-job-hooks nil "Hooks to run when opening a job.")
(defvar egerrit-comment-hooks nil "Hooks to run when opening a comment.")

;;;;;; Faces

(defgroup egerrit-faces nil
  "Faces used by `egerrit'."
  :group 'egerrit
  :group 'faces)

(defface egerrit-error-face
  '((t :inherit error :extend t))
  "Face used to for errors.")

(defface egerrit-inv-error-face
  '((t :inherit next-error))
  "Face used for errors.")

(defface egerrit-success-face
  '((t :inherit success :extend t))
  "Face used to for success.")

(defface egerrit-inv-success-face
  '((t :inherit isearch))
  "Face used to for success.")

(defface egerrit-unknown-face
  '((t :inherit font-lock-comment-face :extend t))
  "Face used to for unknown status.")

(defface egerrit-user-face
  '((t :inherit font-lock-string-face))
  "Face used to highlight a user.")

(defface egerrit-date-face
  '((t :inherit font-lock-comment-face :extend t))
  "Face used to for dates.")

(defface egerrit-project-face
  '((t :inherit font-lock-preprocessor-face :extend t))
  "Face used to for success.")

(defface egerrit-topic-face
  '((t :inherit font-lock-keyword-face :extend t))
  "Face used to for success.")

(defface egerrit-status-face
  '((t :inherit font-lock-variable-name-face :extend t))
  "Face used to for status.")

(defface egerrit-no-review-face
  '((t :inherit egerrit-date-face :extend t ))
  "Face used for missing review.")

(defface egerrit-email-face
  '((t :inherit font-lock-comment-face :extend t ))
  "Face used for email.")

(defface egerrit-file-name-face
  '((t :inherit diff-function :extend nil :weight bold))
  "Face used for file name.")

(defface egerrit-message-face
  '((t :inherit italic :extend t ))
  "Face used for message.")

(defface egerrit-context-highlight-face
  '((nil :inherit region :extend t))
  "Face used for highlight in context.")

(defface egerrit-context-background-face
  '((nil :inherit org-block))
  "Face used for background in context.")

(defface egerrit-heading-face
  '((t :inherit org-level-2))
  "Face used for heading in dashboard.")

(defface egerrit-context-highlight-out-of-focus-face
  `((t :inherit font-lock-comment-face
       :background ,(face-attribute 'default :background)
       :extend t :slant italic :weight light :width condensed))
  "Face used for background in context.")

(defface egerrit-context-block-begin-face
  `((t :inherit org-block-begin-line))
  "Face used for background in context.")

(defface egerrit-context-block-end-face
  `((t :inherit org-block-end-line))
  "Face used for background in context.")

;;;;;; Private

(defvar egerrit--request-curl-options '("--silent")
  "Parameters to `curl'.")
(defvar egerrit--request-buffer " *egerrit-response*"
  "Name of the buffer that holds the response.")
(defvar egerrit--mode 'reviewer
  "Set to either reviewer or author.")
(defvar egerrit--conversation-mode nil
  "Set to either review or feedback.")
(defvar egerrit--switch-branch t
  "Tell `egerrit' to enable branch switching.")

(defvar egerrit--current-query nil
  "Variable that contain the latest `egerrit' query.")
(defvar egerrit--current-changes nil
  "Variable that contain a list of `egerrit-change' objects.")
(defvar egerrit--current-change nil
  "Variable that contain an `egerrit-change'.")
(defvar egerrit--current-revisions nil
  "Variable that contain a list of `egerrit-revision' objects.")
(defvar egerrit--current-revision nil
  "Variable that contain an `egerrit-revision'.")
(defvar egerrit--current-job nil
  "Variable that contain an `egerrit-job'.")
(defvar egerrit--current-jobs nil
  "Variable that contain a list of `egerrit-job'.")
(defvar egerrit--current-conversation nil
  "Variable that contain an `egerrit-conversation'.")
(defvar egerrit--current-comment nil
  "Variable that contain an `egerrit-comment'.")
(defvar egerrit--current-entities nil
  "Variable that contain a list of arbitrary entities.")
(defvar egerrit--current-git-branch nil
  "Name of the current git branch.")
(defvar egerrit--current-dashboard nil
  "Name of the current dashboard buffer.")

(dolist (var '(egerrit--current-query
               egerrit--current-change
               egerrit--current-revisions
               egerrit--current-revision
               egerrit--current-job
               egerrit--current-jobs
               egerrit--current-conversation
               egerrit--current-comment
               egerrit--current-entities
               egerrit--current-git-branch
               egerrit--current-dashboard
               egerrit--mode
               egerrit--conversation-mode))
  (make-variable-buffer-local var))


;;;; Data structures

(cl-defstruct (egerrit-message
               (:constructor egerrit--message-create)
               (:conc-name egerrit--message-))
  (id nil :read-only t)
  (author nil :read-only t)
  (date nil :read-only t)
  (message nil :read-only t)
  (number nil :read-only t))

(cl-defstruct (egerrit-review
               (:constructor egerrit--review-create)
               (:conc-name egerrit--review-))
  (type nil :read-only t)
  (value nil :read-only t)
  (reviewer nil :read-only t))

(cl-defstruct (egerrit-user
               (:constructor egerrit--user-create)
               (:conc-name egerrit--user-))
  (name nil :read-only t)
  (username nil :read-only t)
  (email nil :read-only t))

(cl-defstruct (egerrit-change
               (:constructor egerrit--change-create)
               (:conc-name egerrit--change-))
  (id nil :read-only t)
  (project nil :read-only t)
  (branch nil :read-only t)
  (topic nil :read-only t)
  (changeid nil :read-only t)
  (subject nil :read-only t)
  (status nil :read-only t)
  (created nil :read-only t)
  (updated nil :read-only t)
  (unresolved-comments nil :read-only t)
  (conversations nil :read-only t)
  (number nil :read-only t)
  (reviewers nil :read-only t)
  (labels nil :read-only t)
  (reviews nil :read-only t)
  (owner nil :read-only t)
  (assignee nil :read-only t)
  (messages nil :read-only t)
  (work-in-progress nil :read-only t)
  (current-revision nil :read-only t)
  (revisions nil))

(cl-defstruct (egerrit-revision
               (:constructor egerrit--revision-create)
               (:conc-name egerrit--revision-))
  (id nil :read-only t)
  (number nil :read-only t)
  (created nil :read-only t)
  (uploader nil :read-only t)
  (ref nil :read-only t)
  (fetch nil :read-only t)
  (commit nil :read-only t)
  (subject nil :read-only t)
  (message nil :read-only t)
  (files nil :read-only t)
  (actions nil :read-only t)
  (description nil :read-only t)
  (comments nil)
  (jobs nil))

(cl-defstruct (egerrit-commit
               (:constructor egerrit--commit-create)
               (:conc-name egerrit--commit-))
  (parents nil :read-only t)
  (author nil :read-only t)
  (committer nil :read-only t)
  (subject nil :read-only t)
  (message nil :read-only t))

(cl-defstruct (egerrit-job
               (:constructor egerrit--job-create)
               (:conc-name egerrit--job-))
  (name nil :read-only t)
  (url nil :read-only t)
  (revision nil :read-only t)
  (time nil :read-only t)
  (date nil :read-only t)
  (status nil)
  (project nil)
  (instance nil))

(cl-defstruct (egerrit-comment
               (:constructor egerrit--comment-create)
               (:conc-name egerrit--comment-))
  (id nil :read-only t)
  (file nil :read-only t)
  (patch-set nil :read-only t)
  (author nil :read-only t)
  (side nil :read-only t)
  (path nil :read-only t)
  (line nil :read-only t)
  (range nil :read-only t)
  (in-reply-to nil :read-only t)
  (updated nil :read-only t)
  (message)
  (context nil :read-only t)
  (unresolved nil :read-only t)
  (region nil)
  (project nil))

(cl-defstruct (egerrit-range
               (:constructor egerrit--range-create)
               (:conc-name egerrit--range-))
  (start-line nil)
  (start-character nil)
  (end-line nil)
  (end-character nil))

(cl-defstruct (egerrit-conversation
               (:constructor egerrit--conversation-create)
               (:conc-name egerrit--conversation-))
  (id nil :read-only t)
  (file nil :read-only t)
  (region nil)
  (location nil) ; range
  (patch-set nil :read-only t)
  (comments nil)
  (status nil)
  (side nil :read-only t)
  (range nil :read-only t)
  (context-lines nil :read-only t)
  (context nil))

(cl-defstruct (egerrit-fetch
               (:constructor egerrit--fetch-create)
               (:conc-name egerrit--fetch-))
  (ssh nil :read-only t)
  (http nil :read-only t))

(cl-defstruct (egerrit-download
               (:constructor egerrit--download-create)
               (:conc-name egerrit--download-))
  (url nil :read-only t)
  (ref nil :read-only t)
  (checkout nil :read-only t)
  (cherry-pick nil :read-only t)
  (format-patch nil :read-only t)
  (pull nil :read-only t))

;;;; Functions

;;;;; Data interface

(defun egerrit-get-detailed-change (change)
  "Return a detailed `egerrit-change' object for CHANGE."
  (let* ((changeid (url-unhex-string (egerrit--change-id change)))
         (json-entity (elt (egerrit--read-change changeid) 0)))
    (thread-last (egerrit--create-detailed-change json-entity)
                 (funcall egerrit-job-function))))

(defun egerrit-get-changes (query)
  "Return a list of `egerrit-change' objects based on QUERY."
  (thread-last query
               (seq-map (lambda (it)
                          (let ((changes (seq-map #'egerrit--create-change (apply #'egerrit--read-changes (cdr it)))))
                            (when changes
                              `(,(car it) ; Header line
                                ,@changes
                                "" ; Empty line
                                )))))
               (flatten-list)
               (seq-remove #'null)
               (butlast)))

(defun egerrit-get-conversations (changeid)
  "Return a list of conversations on CHANGEID."
  (let* ((json-comments (egerrit--read-comments changeid)))
    (thread-last json-comments
                 (seq-map #'egerrit--remove-blocked-comments)
                 (seq-map #'egerrit--group-conversation-comments)
                 (seq-map #'egerrit--create-conversations)
                 (flatten-list))))

(defun egerrit-put-draft-comment (change revision comment)
  "Put a draft COMMENT CHANGE's REVISION."
  (let ((url (concat "https://" egerrit-request-url "/a/changes/" (egerrit--change-id change)
                     "/revisions/" (symbol-name (egerrit--revision-id revision)) "/drafts")))
    (egerrit-request
     url
     :type "PUT"
     :header '(("Content-Type" . "application/json"))
     :auth t
     :data comment
     :parser #'egerrit-request-parser)))

(defun egerrit-post-draft-comments (change revision)
  "Post draft comments on CHANGE's REVISION."
  (let ((url (concat "https://" egerrit-request-url "/a/changes/" (egerrit--change-id change)
                     "/revisions/" (symbol-name (egerrit--revision-id revision)) "/review"))
        (review '(("drafts" . "PUBLISH_ALL_REVISIONS"))))
    (egerrit-request
     url
     :type "POST"
     :header '(("Content-Type" . "application/json"))
     :auth t
     :data review)))

(defun egerrit-post-review-label (change revision label &optional review-message)
  "Post LABEL on CHANGE's REVISION.

Optionally send a REVIEW-MESSAGE."
  (let* ((url (concat "https://" egerrit-request-url "/a/changes/" (egerrit--change-id change)
                      "/revisions/" (symbol-name (egerrit--revision-id revision)) "/review"))
         (review `(("labels" . ((,(car label) . ,(cdr label)))))))
    (when review-message
      (push `("message" . ,review-message) review))
    (egerrit-request
     url
     :type "POST"
     :header '(("Content-Type" . "application/json"))
     :auth t
     :data review)))

(defun egerrit-review-groups ()
  "Return a list of all review groups."
  (let* ((url (concat "https://" egerrit-request-url "/a/groups/"))
         (response
          (egerrit-request
           url
           :auth t
           :type "GET"
           :header '(("Content-Type" . "application/json"))
           :parser #'egerrit-request-parser)))
    (thread-last response
                 (seq-map #'car)
                 (seq-map #'symbol-name))))

(defun egerrit-all-users (&optional name-contain)
  "Return a list of all users.
Optional argument NAME-CONTAIN which limits the search space."
  (let ((users)
        (response)
        (number-of-responses 0)
        (max-number-of-users egerrit-max-changes)
        (all-users-received nil))
    (while (not all-users-received)
      (setq response
            (egerrit--read-users
             max-number-of-users
             (* max-number-of-users number-of-responses)
             name-contain))
      (setq number-of-responses (1+ number-of-responses))
      (unless (let-alist (seq-first (seq-reverse response)) ._more_accounts)
        (setq all-users-received t))
      (push
       (thread-last response
                    (seq-filter (lambda (it) (let-alist it .email)))
                    (seq-map #'egerrit--create-user))
       users))
    (thread-last users
                 (seq-reverse)
                 (flatten-list))))

(defun egerrit-users (&optional name-contain)
  "Return a list of all current users.
Optional argument NAME-CONTAIN which limits the search space."
  (let* ((users (egerrit-all-users name-contain))
         (name-length
          (thread-last users
                       (seq-map (lambda (it)
                                  (length (egerrit--user-name it))))
                       (seq-max))))
    name-length
    (thread-last users
                 (seq-map (lambda (it)
                            (let ((name (egerrit--pad-string (egerrit--user-name it) name-length ?\s))
                                  (email (egerrit--user-email it)))
                              (put-text-property 0 (length email) 'face 'egerrit-email-face email)
                              `(,(format "%s  %s" name email) . ,email)))))))

;;;;; Request

(cl-defun egerrit-request-start-process (process-name
                                         command
                                         &key
                                         (success-message "Finished"))
  "Start a process named PROCESS-NAME and execute COMMAND.

Send message SUCCESS-MESSAGE upon success."
  (let* ((program shell-file-name)
         (buffer (format " *%s*" process-name))
         (args `(,process-name ,buffer ,program "-c" ,command)))
    (with-current-buffer (get-buffer-create buffer)
      (setf (buffer-string) ""))
    (set-process-sentinel
     (apply #'start-file-process args)
     (lambda (_ event)
       (cond ((string-match-p "finished" event)
              (message success-message))
             (t (pop-to-buffer buffer)))))))

(cl-defun egerrit-request (url &key
                               params
                               data
                               auth
                               type
                               header
                               parser)
  "Perform a REST API request to URL.

The following keys are optional.
PARAMS: Parameters in the request.
DATA: An alist which will be encoded as a json object.
AUTH: A boolean to enable or disable authentication.
TYPE: POST, PUT, DELETE etc.
HEADER: What kind of header to use.
PARSER: Optional parser to apply to the response."
  (with-current-buffer (get-buffer-create egerrit--request-buffer)
    (erase-buffer)
    (apply #'call-process
           `("curl" nil t nil
             ,@(egerrit--request-args
                url
                :type type
                :auth auth
                :params params
                :header header
                :data data
                :curl-options egerrit--request-curl-options)))
    (when parser
      (funcall parser))))

(defun egerrit-request-parser ()
  "Default parser for Gerrit messages."
  (goto-char (point-min))
  (when (string-equal ")]}'\n" (thing-at-point 'line))
    (forward-line)
    (json-parse-buffer :array-type 'array
                       :object-type 'alist
                       :null-object nil
                       :false-object nil)))

;;;;; Accessors

(cl-defgeneric egerrit-updated (entity)
  "Return time ENTITY was last updated.")

(cl-defmethod egerrit-updated ((change egerrit-change))
  "Return time CHANGE was last updated."
  (egerrit--change-updated change))

(cl-defmethod egerrit-updated ((revision egerrit-revision))
  "Return time REVISION was last updated."
  (egerrit--revision-created revision))

(cl-defmethod egerrit-updated ((job egerrit-job))
  "Return time JOB was last updated."
  (egerrit--job-date job))

(cl-defgeneric egerrit-url (entity)
  "Return URL to ENTITY.")

(cl-defmethod egerrit-url ((change egerrit-change))
  "Return URL to CHANGE."
  (let ((project (egerrit--change-project change))
        (number (egerrit--change-number change)))
    (format "https://%s/c/%s/+/%s" egerrit-request-url project number)))

(cl-defmethod egerrit-url ((job egerrit-job))
  "Return URL to JOB."
  (format "%sconsoleText" (egerrit--job-url job)))

(cl-defmethod egerrit-url ((revision egerrit-revision))
  "Return URL to REVISION."
  (let* ((change egerrit--current-change)
         (project (egerrit--change-project change))
         (change-number (egerrit--change-number change))
         (revision-number (egerrit--revision-number revision)))
    (format "https://%s/c/%s/+/%s/%s" egerrit-request-url project change-number revision-number)))

(cl-defgeneric egerrit-owner (entity)
  "Return the owner of ENTITY.")

(cl-defmethod egerrit-owner ((change egerrit-change))
  "Return the owner of CHANGE."
  (egerrit--user-name
   (egerrit--change-owner change)))

(cl-defmethod egerrit-owner ((revision egerrit-revision))
  "Return the owner of a REVISION."
  (egerrit--user-name (egerrit--revision-uploader revision)))

(cl-defgeneric egerrit-project (entity)
  "Return the project of ENTITY.")

(cl-defmethod egerrit-project ((change egerrit-change))
  "Return CHANGE's project."
  (egerrit--change-project change))

(cl-defgeneric egerrit-subject (entity)
  "Return the subject of ENTITY.")

(cl-defmethod egerrit-subject ((change egerrit-change))
  "Return CHANGE's subject."
  (egerrit--change-subject change))

(cl-defgeneric egerrit-fetch (entity)
  "Return the fetch info for ENTITY.")

(cl-defmethod egerrit-fetch ((change egerrit-change))
  "Return CHANGE's fetch info."
  (let ((current-revision-id (egerrit--change-current-revision change))
        (revisions (egerrit--change-revisions change)))
    (egerrit-fetch
     (seq-find
      (lambda (revision)
        (string=
         current-revision-id
         (symbol-name
          (egerrit--revision-id revision))))
      revisions))))

(cl-defmethod egerrit-fetch ((revision egerrit-revision))
  "Return REVISION's fetch info."
  (egerrit--revision-fetch revision))

(cl-defgeneric egerrit-job-status (entity)
  "Return the status of the ENTITY.")

(cl-defmethod egerrit-job-status ((job egerrit-job))
  "Return the status of JOB."
  (egerrit--job-status job))

(cl-defgeneric egerrit-region (entity)
  "Return region for ENTITY.")

(cl-defmethod egerrit-region ((comment egerrit-comment))
  "Return region for COMMENT."
  (egerrit--comment-region comment))

(cl-defmethod egerrit-region ((conversation egerrit-conversation))
  "Return region for CONVERSATION."
  (egerrit--conversation-region conversation))

(cl-defmethod egerrit-annotator-config (_entity)
  "Return annotation configuration for ENTITY.")

(cl-defgeneric egerrit-annotator-config ((_job egerrit-job))
  "Return annotation configuration for JOB."
  egerrit-job-config)

(cl-defgeneric egerrit-annotator-config ((_revision egerrit-revision))
  "Return annotation configuration for revision."
  egerrit-revision-config)

(cl-defgeneric egerrit-annotator-config ((_change egerrit-change))
  "Return annotation configuration for change."
  egerrit-dashboard-config)

;;;;; Other

(defun egerrit-get-project-root (entity)
  "Return the project root for ENTITY."
  (let ((remote (file-remote-p default-directory))
        (root (pcase-let* ((project (egerrit-project entity))
                           (`(,_ . ,path) (assoc project egerrit-project-roots)))
                path)))
    (if root
        (if remote
            (concat remote root)
          root)
      root
      (message "No root found for project: %s" (egerrit-project entity)))))

(defun egerrit-review (change type)
  "Return the value for the CHANGE's review TYPE."
  (let* ((reviews (egerrit--change-reviews change)))
    (thread-last reviews
                 (seq-find (lambda (review)
                             (eq type
                                 (egerrit--review-type review))))
                 (egerrit--review-value))))

(defun egerrit-job-content (job)
  "Return content of JOB."
  (let* ((url (egerrit-url job))
         (egerrit--request-curl-options '("--silent" "--insecure")))
    (egerrit-request
     url
     :type "GET"
     :header '(("Content-Type" . "application/json"))
     :parser (lambda () (buffer-string)))))

(defun egerrit-get-buffer (name)
  "Return an `egerrit' buffer with NAME."
  (let* ((directory default-directory)
         (host (or (file-remote-p default-directory 'host) ""))
         (buffer-name
          (if (string= host "")
              (format "*%s*" name)
            (format "*%s <%s>*" name host)))
         (buffer-candidate
          (thread-last (buffer-list)
                       (seq-map #'buffer-name)
                       (seq-find (lambda (it) (string= it buffer-name))))))
    (if buffer-candidate
        (get-buffer buffer-candidate)
      (let ((buffer (generate-new-buffer buffer-name)))
        (with-current-buffer buffer
          (setq-local default-directory directory))
        buffer))))

(defun egerrit-custom-query ()
  "Read custom query."
  (let ((query (read-string "Insert query: ")))
    `((,query . (,query)))))

(defun egerrit-query-from-url ()
    "Create a Gerrit query based on an URL."
    (let ((url (read-string "Enter URL: "))
          (re (rx-to-string `(and bol (regexp ".*")
                                  ,egerrit-request-url
                                  "/c/" (group (regexp ".*?"))
                                  "/+/" (group (one-or-more digit)))))
          (query))
      (string-match re url)
      (setq query
            (format "project:%s %s"
                    (match-string 1 url)
                    (match-string 2 url)))
      `((,query . (,query)))))

(defun egerrit-select-revision (change &optional revisions)
  "Select one of CHANGE's REVISIONS."
  (when-let ((revisions
              (or revisions
                  (egerrit--change-revisions change))))
    (let* ((candidates
            (thread-last revisions
                         (seq-reverse)
                         (seq-map
                          (lambda (it)
                            (egerrit--completing-read-annotator it)))))
           (metadata `(metadata
                       (category . egerrit-revision)
                       (cycle-sort-function . identity)
                       (display-sort-function . identity)))
           (collection (lambda (string predicate action)
                         (if (eq action 'metadata)
                             metadata
                           (complete-with-action action candidates string predicate))))
           (cand (completing-read "Select revision: " collection nil t)))
      (cdr (assoc cand candidates)))))

(defun egerrit-select-job (jobs)
  "Select a job from list of JOBS."
  (let* ((candidates
          (thread-last jobs
                       (egerrit--sort-jobs)
                       (seq-map
                        (lambda (it)
                          (egerrit--completing-read-annotator it)))))
         (metadata `(metadata
                     (category . egerrit-job)
                     (cycle-sort-function . identity)
                     (display-sort-function . identity)))
         (collection (lambda (string predicate action)
                       (if (eq action 'metadata)
                           metadata
                         (complete-with-action action candidates string predicate))))
         (cand (completing-read "Select job: " collection nil t)))
    (cdr (assoc cand candidates))))

(defun egerrit-open-info ()
  "Open info buffer about change."
  (interactive)
  (let ((change (pcase major-mode
                  ('egerrit-dashboard-mode (tabulated-list-get-id))
                  ('egerrit-diff-mode egerrit--current-change)
                  ('egerrit-conversation-mode egerrit--current-change)
                  ('egerrit-job-mode egerrit--current-change)
                  (_ nil))))
    (when (egerrit-change-p change)
      (let* ((change (egerrit-get-detailed-change change))
             (revision
              (if current-prefix-arg
                  (egerrit-select-revision change)
                (seq-first (seq-reverse (egerrit--change-revisions change)))))
             (buffer (egerrit-get-buffer "egerrit-info")))

        (with-current-buffer buffer
          (let ((inhibit-read-only t))
            (erase-buffer)
            (insert (egerrit--info-str change revision)))
          (goto-char (point-min))
          (read-only-mode)
          (pop-to-buffer (current-buffer)))))))

(defun egerrit--info-str (change revision)
  "Generate an info string about CHANGE's REVISION."
  (let ((info-heading "Change info")
        (requirements-heading "Submit requirements")
        (relation-heading "Relation chain")
        (has-merge-conflicts
         (not (egerrit-get-mergeable change)))
        (related-changes
         (egerrit-get-related-changes change revision))
        (content))

    (put-text-property 0 (length info-heading) 'face 'egerrit-heading-face info-heading)
    (put-text-property 0 (length relation-heading) 'face 'egerrit-heading-face relation-heading)
    (put-text-property 0 (length requirements-heading) 'face 'egerrit-heading-face requirements-heading)

    (setq content
          `(,(format "%s\n\n" info-heading)
            ,(when has-merge-conflicts
               (let ((str "Merge Conflict"))
                 (put-text-property 0 (length str) 'face 'egerrit-inv-error-face str)
                 (format "%s\n" str)))
            ,(egerrit--info-entry-str "Subject" (egerrit--subject-str change))
            ,(egerrit--info-entry-str "Owner" (egerrit--owner-str change))
            ,(egerrit--info-entry-str "Reviewers"
                                      (string-join (seq-map
                                                    (lambda (it)
                                                      (alist-get 'name it))
                                                    (alist-get 'REVIEWER (egerrit--change-reviewers change)))
                                                   ", ")
                                      'egerrit-unknown-face)
            ,(egerrit--info-entry-str "CC"
                                      (string-join (seq-map
                                                    (lambda (it)
                                                      (alist-get 'name it))
                                                    (alist-get 'CC (egerrit--change-reviewers change)))
                                                   ", ")
                                      'egerrit-unknown-face)
            ,(format "\n%s\n\n" requirements-heading)
            ,(format "%s" (egerrit--change-requirements-str change))
            ,(when related-changes
               (format "\n%s\n\n%s\n" relation-heading (string-join related-changes "\n")))))

    (string-join (seq-remove #'null content) "")))

(defun egerrit--info-entry-str (key value &optional value-face)
  "Return a string of KEY VALUE.

Optionally add VALUE-FACE to value."
  (let ((key-str (format "%s: " key)))
    (put-text-property 0 (length key-str) 'face 'egerrit-unknown-face key-str)
    (when value-face
      (put-text-property 0 (length value) 'face value-face value))
    (format "%s%s\n" key-str value)))

(defun egerrit--info-review-str (labels)
  "Return a review string from LABELS."
  (thread-last labels
               (seq-map (lambda (it)
                          (let-alist it
                            (unless (= (or .value 0) 0)
                              (let ((name-str .name)
                                    (value-str (format "(%s)" .value)))
                                (put-text-property 0 (length value-str)
                                                   'face (if (> .value 0) 'egerrit-inv-success-face 'egerrit-inv-error-face)
                                                   value-str)
                                (format "%s %s" name-str value-str))))))
               (seq-remove #'null)))

(defun egerrit--change-requirements-str (change)
  "Return a requirement string for CHANGE."
  (let ((code-reviews
         (egerrit--info-review-str
          (let-alist (egerrit--change-labels change)  .Code-Review.all)))
        (secondary-reviews (egerrit--info-review-str
                            (let-alist (egerrit--change-labels change)  .Secondary-Review.all)))
        (verified
         (egerrit--info-review-str
          (let-alist (egerrit--change-labels change)  .Verified.all))))
    (concat
     (egerrit--info-entry-str "Code-Review" (string-join code-reviews ", "))
     (egerrit--info-entry-str "Secondary-Review" (string-join secondary-reviews ", "))
     (egerrit--info-entry-str "Verified" (string-join verified ", ")))))

(defun egerrit-get-related-changes (change revision)
  "Get a list of related change(s) for CHANGE's REVISION."
  (let* ((change-id (egerrit--change-id change))
         (revision-id (symbol-name (egerrit--revision-id revision)))
         (url (concat "https://" egerrit-request-url "/a/changes/" change-id
                      "/revisions/" revision-id "/related"))
         (related-changes
          (egerrit-request
           url
           :type "GET"
           :header '(("Content-Type" . "application/json"))
           :parser #'egerrit-request-parser
           :auth t)))
    (seq-map (lambda (change)
               (let-alist change .commit.subject))
             (alist-get 'changes related-changes))))

(defun egerrit-get-mergeable (change)
  "Get a response from Gerrit if CHANGE is mergeable."
  (let* ((change-id (egerrit--change-id change))
         (revision-id (egerrit--change-current-revision change))
         (url (concat "https://" egerrit-request-url "/a/changes/" change-id
                      "/revisions/" revision-id "/mergeable"))
         (mergeable
          (egerrit-request
           url
           :type "GET"
           :header '(("Content-Type" . "application/json"))
           :parser #'egerrit-request-parser
           :auth t)))
    (alist-get 'mergeable mergeable)))

;;;;; Dashboard

(defun egerrit-dashboard-create-imenu-index ()
  "Create an `imenu' index for the dashboard."
  (let ((index))
    (goto-char (point-min))
    (while (not (eobp))
      (let ((change (tabulated-list-get-id)))
        (unless (or (stringp change)
                    (null change))
          (push `(,(egerrit-imenu-entry change) . ,(point))
                index)))
      (forward-line 1))
    (seq-reverse index)))

(cl-defgeneric egerrit-imenu-entry (entity)
  "Return an `imenu' entry for ENTITY.")

(cl-defmethod egerrit-imenu-entry ((change egerrit-change))
  "Return an `imenu' entry for CHANGE."
  (let ((config (seq-find (lambda (it)
                            (when (eq 'egerrit--subject-str (plist-get it :function))
                              (plist-get it :length)))
                          egerrit-dashboard-config)))
    (if config
        (format "%s %s"
                (egerrit--pad-string (egerrit--subject-str change)
                                     (plist-get config :length) ?\s)
                (egerrit--owner-str change))
      (format "%s %s"
              (egerrit--subject-str change)
              (egerrit--owner-str change)))))

;;;; Commands
;;;;; Views

;;;###autoload
(defun egerrit-dashboard ()
  "Open the `egerrit' dashboard."
  (interactive)
  (pop-to-buffer-same-window
   (egerrit-get-buffer "egerrit-dashboard"))
  (erase-buffer)
  (insert "***Important Message***\nDevelopment of egerrit has migrated from gitlab to sourcehut!\nThe project can be found at: https://git.sr.ht/~niklaseklund/egerrit\nUpdate your package config to track the correct repository"))

;;;###autoload
(defun egerrit-refresh-dashboard ()
  "Refresh the dashboard."
  (interactive)
  (let ((current-change (tabulated-list-get-id)))
    (setq egerrit--current-changes (egerrit-get-changes egerrit--current-query))
    (let* ((tabulated-list-entries
            (seq-map #'egerrit-get-change-entry egerrit--current-changes)))
      (tabulated-list-print t))
    ;; Navigate back to current change
    (when (egerrit-change-p current-change)
      (goto-char (point-min))
      (while (and (not (eobp))
                  (not
                   (let ((change (tabulated-list-get-id)))
                     (when (egerrit-change-p change)
                       (string= (egerrit--change-id change)
                                (egerrit--change-id current-change))))))
        (forward-line 1))
      ;; If not found, move to beginning of buffer
      (when (eobp) (goto-char (point-min))))))

;;;###autoload
(defun egerrit-select-dashboard-view ()
  "Update the dashboard to show another view."
  (interactive)
  (when-let* ((selected
               (completing-read "Select view: " egerrit-changes-queries))
              (query (alist-get selected egerrit-changes-queries nil nil #'string=)))
    (if (functionp query)
        (setq egerrit--current-query (funcall query))
      (setq egerrit--current-query query))
    (egerrit-refresh-dashboard)))

;;;;; Change

;;;###autoload
(defun egerrit-set-review-label (&optional message)
  "Set review label for CHANGE.

Optionally add a MESSAGE."
  (interactive "P")
  (when-let* ((change (cond ((eq major-mode 'egerrit-dashboard-mode) (tabulated-list-get-id))
                            ((eq major-mode 'egerrit-conversation-mode) egerrit--current-change)
                            ((eq major-mode 'egerrit-diff-mode) egerrit--current-change)
                            (t nil)))
              (detailed-change (egerrit-get-detailed-change change))
              (revision (seq-first (seq-reverse (egerrit--change-revisions detailed-change))))
              (candidates (egerrit--review-label-candidates detailed-change))
              (metadata `(metadata
                          (cycle-sort-function . identity)
                          (display-sort-function . identity)))
              (collection (lambda (string predicate action)
                            (if (eq action 'metadata)
                                metadata
                              (complete-with-action action candidates string predicate))))
              (review (completing-read "Select: " collection nil t)))
    (egerrit-post-review-label
     detailed-change revision
     (cdr (assoc review candidates))
     (when message
       (read-string "Enter message: ")))))

(defun egerrit--review-label-candidates (change)
  "Return possible review labels for CHANGE."
  (let* ((labels (egerrit--change-labels change))
         (code-review-values (let-alist labels .Code-Review.values))
         (secondary-review-values (let-alist labels .Secondary-Review.values)))

    (when egerrit-user-name
      ;; Code-Review
      (when-let ((user-settings (seq-find (lambda (it)
                                            (string= (alist-get 'username it) egerrit-user-name))
                                          (let-alist labels .Code-Review.all)))
                 (range (let-alist user-settings .permitted_voting_range)))
        ;; Filter code review values
        (setq code-review-values
              (thread-last code-review-values
                           (seq-filter (lambda (it)
                                         (let ((value (string-to-number (symbol-name (car it)))))
                                           (and (<= value (let-alist range .max))
                                                (>= value (let-alist range .min)))))))))

      ;; Secondary-Review
      (when-let ((user-settings (seq-find (lambda (it)
                                            (string= (alist-get 'username it) egerrit-user-name))
                                          (let-alist labels .Secondary-Review.all)))
                 (range (let-alist user-settings .permitted_voting_range)))
        ;; Filter secondary review values
        (setq secondary-review-values
              (thread-last secondary-review-values
                           (seq-filter (lambda (it)
                                         (let ((value (string-to-number (symbol-name (car it)))))
                                           (and (<= value (let-alist range .max))
                                                (>= value (let-alist range .min))))))))))

    ;; Create a list of options
    `(,@(seq-map (lambda (it)
                   (let ((value (symbol-name (car it)))
                         (description (cdr it)))
                     (put-text-property 0 (length value)
                                        'face
                                        (cond ((> (string-to-number value) 0) 'egerrit-inv-success-face)
                                              ((< (string-to-number value) 0) 'egerrit-inv-error-face)
                                              (t 'egerrit-unknown-face))
                                        value)
                     (put-text-property 0 (length description) 'face 'egerrit-unknown-face description)
                     `(,(format "SR: %s %s" value description) . ("Secondary-Review" . ,value))))
                 secondary-review-values)
      ,@(seq-map (lambda (it)
                   (let ((value (symbol-name (car it)))
                         (description (cdr it)))
                     (put-text-property 0 (length value)
                                        'face
                                        (cond ((> (string-to-number value) 0) 'egerrit-inv-success-face)
                                              ((< (string-to-number value) 0) 'egerrit-inv-error-face)
                                              (t 'egerrit-unknown-face))
                                        value)
                     (put-text-property 0 (length description) 'face 'egerrit-unknown-face description)

                     `(,(format "CR: %s %s" value  description) . ("Code-Review" . ,value))))
                 code-review-values))))


;;;###autoload
(defun egerrit-custom-message (change)
  "Send custom message to CHANGE."
  (interactive
   (list (tabulated-list-get-id)))
  (let ((detailed-change (egerrit-get-detailed-change change))
        (message (read-string "Message to send: ")))
    (egerrit--post-message message detailed-change)))

;;;###autoload
(defun egerrit-download-change ()
  "Download change."
  (interactive)
  (let ((change (pcase major-mode
                  ('egerrit-dashboard-mode (tabulated-list-get-id))
                  ('egerrit-diff-mode egerrit--current-change)
                  ('egerrit-conversation-mode egerrit--current-change)
                  ('egerrit-job-mode egerrit--current-change)
                  (_ nil))))
    (when (egerrit-change-p change)
      (let* ((default-directory (egerrit-get-project-root change))
             (detailed-change (egerrit-get-detailed-change change))
             (command (format "%s && %s"
                              (egerrit--download-checkout
                               (egerrit--fetch-ssh
                                (egerrit-fetch detailed-change)))
                              (egerrit--git-switch-command (egerrit--branch-name change)))))
        (egerrit-request-start-process
         "egerrit-change-download"
         command
         :success-message (format "Finished downloading change: %s"
                                  (egerrit--change-number detailed-change)))))))

;;;###autoload
(defun egerrit-cherry-pick-change (change)
  "Cherry pick CHANGE."
  (interactive
   (list (tabulated-list-get-id)))
  (when (egerrit-change-p change)
    (let* ((default-directory (egerrit-get-project-root change))
           (detailed-change (egerrit-get-detailed-change change))
           (revision (if current-prefix-arg
                         (egerrit-select-revision detailed-change)
                       (seq-first (seq-reverse (egerrit--change-revisions detailed-change)))))
           (command (format "%s && %s"
                            (egerrit--download-cherry-pick
                             (egerrit--fetch-ssh
                              (egerrit-fetch detailed-change)))
                            (egerrit--git-switch-command (egerrit--branch-name change revision)))))
      (egerrit-request-start-process
       "*egerrit-change-cherry-pick*"
       command
       :success-message (format "Finished cherry picking change: %s"
                                (egerrit--change-number detailed-change))))))

;;;###autoload
(defun egerrit-add-review-group (change)
  "Add a review group to CHANGE."
  (interactive (list (tabulated-list-get-id)))
  (let* ((group
          (completing-read "Select a group: "
                           (egerrit-review-groups))))
    (egerrit--post-reviewer group change)))

;;;###autoload
(defun egerrit-add-reviewer (change)
  "Add a reviewer to CHANGE."
  (interactive (list (tabulated-list-get-id)))
  (let* ((name-contain (char-to-string (read-char "Select a user: ")))
         (users (egerrit-users name-contain))
         (user (completing-read "Select a user: " users nil t name-contain))
         (reviewer (cdr (assoc user users))))
    (egerrit--post-reviewer reviewer change)))

;;;###autoload
(defun egerrit-add-cc-user (change)
  "Add a user as cc to CHANGE."
  (interactive (list (tabulated-list-get-id)))
  (let* ((name-contain (char-to-string (read-char "Select a user: ")))
         (users (egerrit-users name-contain))
         (reviewer (cdr (assoc (completing-read "Select a user: " users nil t name-contain) users))))
    (egerrit--post-cc reviewer change)))

;;;###autoload
(defun egerrit-add-cc-group (change)
  "Add a cc group to CHANGE."
  (interactive (list (tabulated-list-get-id)))
  (let* ((group
          (completing-read "Select a group: "
                           (egerrit-review-groups))))
    (egerrit--post-cc group change)))

;;;###autoload
(defun egerrit-add-hashtag (change)
  "Add hashtag to CHANGE."
  (interactive (list (tabulated-list-get-id)))
  (let* ((hash-tag
          (read-string "Add hashtag: "))
         (id (egerrit--change-id change))
         (url (concat "https://" egerrit-request-url "/a/changes/" id "/hashtags"))
         (data `(("add" . [,hash-tag]))))
    (egerrit-request
     url
     :type "POST"
     :header '(("Content-Type" . "application/json"))
     :data data
     :auth t)))

;;;###autoload
(defun egerrit-toggle-abandon-change (change)
  "Toggle status abandon on CHANGE."
  (interactive (list (tabulated-list-get-id)))
  (if (string= (egerrit--change-status change) "ABANDONED")
      ;; Restore
      (let* ((id (egerrit--change-id change))
             (url (concat "https://" egerrit-request-url "/a/changes/" id "/restore")))
        (egerrit-request
         url
         :type "POST"
         :auth t))
    ;; Abandon
    (let* ((id (egerrit--change-id change))
           (url (concat "https://" egerrit-request-url "/a/changes/" id "/abandon")))
      (egerrit-request
       url
       :type "POST"
       :auth t))))

;;;###autoload
(defun egerrit-set-topic (change)
  "Set or remove topic to CHANGE."
  (interactive (list (tabulated-list-get-id)))
  (let ((topic (read-string "Write topic: " (egerrit--change-topic change))))
    (if (string-empty-p topic)
        (let* ((id (egerrit--change-id change))
               (url (concat "https://" egerrit-request-url "/a/changes/" id "/topic")))
          (egerrit-request
           url
           :type "DELETE"
           :auth t))
      (let* ((id (egerrit--change-id change))
             (url (concat "https://" egerrit-request-url "/a/changes/" id "/topic"))
             (data `(("topic" . ,topic))))
        (egerrit-request
         url
         :type "PUT"
         :header '(("Content-Type" . "application/json"))
         :auth t
         :data data)))))

;;;###autoload
(defun egerrit-change-toggle-wip (change)
  "Toggle WIP status on CHANGE."
  (interactive (list (egerrit-get-detailed-change (tabulated-list-get-id))))
  (if (egerrit--change-work-in-progress change)
      (let* ((id (egerrit--change-id change))
             (url (concat "https://" egerrit-request-url "/a/changes/" id "/ready")))
        (egerrit-request
         url
         :type "POST"
         :auth t))
    (let* ((id (egerrit--change-id change))
           (url (concat "https://" egerrit-request-url "/a/changes/" id "/wip")))
      (egerrit-request
       url
       :type "POST"
       :auth t))))

;;;###autoload
(defun egerrit-rebase-change (change)
  "Rebase CHANGE."
  ;; TODO(Niklas Eklund, 20220413): Rename to rebase master and add a
  ;; rebase on change command. Inside there we can make a query for
  ;; non detailed changes, related to current project
  (interactive (list (tabulated-list-get-id)))
  (let* ((id (egerrit--change-id change))
         (url (concat "https://" egerrit-request-url "/a/changes/" id "/rebase")))
    (egerrit-request
     url
     :type "POST"
     :auth t)))

;;;###autoload
(defun egerrit-set-assignee (change)
  "Set or remove user as assignee on CHANGE."
  (interactive (list
                (egerrit-get-detailed-change
                 (tabulated-list-get-id))))
  (if (egerrit--change-assignee change)
      (let* ((id (egerrit--change-id change))
             (url (concat "https://" egerrit-request-url "/a/changes/" id "/assignee")))
        (egerrit-request
         url
         :type "DELETE"
         :auth t))
    (let* ((name-contain (char-to-string (read-char "Select a user: ")))
           (url (concat "https://" egerrit-request-url "/a/changes/" (egerrit--change-id change) "/assignee"))
           (users (egerrit-users name-contain))
           (assignee (cdr (assoc (completing-read "Select a user:" users nil t name-contain) users)))
           (data `(("assignee" . ,assignee))))
      (egerrit-request
       url
       :type "PUT"
       :header '(("Content-Type" . "application/json"))
       :auth t
       :data data))))

;;;;; Revision

;;;###autoload
(defun egerrit-open-conversations (change)
  "Open CHANGE's conversations."
  (interactive
   (list (tabulated-list-get-id)))
  (let* ((detailed-change (egerrit-get-detailed-change change))
         (revision
          (seq-first (seq-reverse (egerrit--change-revisions detailed-change))))
         (conversations (egerrit--get-conversations detailed-change revision))
         (buffer (egerrit-get-buffer "egerrit-conversations")))
    (if conversations
        (with-current-buffer buffer
          (let ((inhibit-read-only t))
            (erase-buffer)
            (egerrit-conversation-mode)
            (egerrit--generate-comments-buffer conversations)
            (setq egerrit--conversation-mode 'feedback)
            (setq egerrit--current-change detailed-change)
            (setq egerrit--current-revision revision)
            (setq-local default-directory (egerrit-get-project-root detailed-change)))
          (read-only-mode)
          (goto-char (point-min))
          (pop-to-buffer-same-window (current-buffer))
          (when-let ((comments (text-property-search-forward 'comments t t)))
            (goto-char (prop-match-beginning comments))
            (recenter))
          (egerrit-focus-on-conversation)
          (egerrit-visit-conversation-source-code t))
      (message "Revision has no comments."))))

;;;###autoload
(defun egerrit-open-conversations-buffer ()
  "Open conversations buffer."
  (interactive)
  (pop-to-buffer
   (egerrit-get-buffer "egerrit-conversations")))

;;;###autoload
(defun egerrit-create-comment ()
  "Add or update a comment.
This function will work both for review/feedback comments."
  (interactive)
  (let* ((conversation-id (plist-get (text-properties-at (point)) 'conversation-id))
         (conversation
          (if (eq major-mode 'egerrit-diff-mode)
              (egerrit--diff-review-comment)
            (seq-find (lambda (it)
                        (string= (egerrit--conversation-id it) (symbol-name conversation-id)))
                      egerrit--current-entities))))
    (with-current-buffer (egerrit-get-buffer "egerrit-comment")
      (erase-buffer)
      (run-hooks 'egerrit-comment-hooks)
      (egerrit-comment-mode)
      (setq egerrit--current-conversation conversation)
      (when-let ((reply
                  (alist-get "Reply" (egerrit--conversation-comments conversation) nil nil #'string=)))
        (insert (substring-no-properties reply 0 (length reply))))
      (pop-to-buffer (current-buffer)))))

;;;###autoload
(defun egerrit-complete-comment ()
  "Finalize the comment and refresh the comments buffer."
  (interactive)
  (let* ((conversation egerrit--current-conversation)
         (id (egerrit--conversation-id conversation))
         (comments (egerrit--conversation-comments conversation))
         (message (buffer-substring-no-properties (point-min) (point-max)))
         (updated nil))

    ;; Update or add a reply
    (if (string= (car (seq-first (seq-reverse comments))) "Reply")
        (setf (cdr (seq-first (seq-reverse comments))) message)
      (setq comments `(,@comments ("Reply" . ,message))))
    (setf (egerrit--conversation-comments conversation) comments)

    (kill-buffer-and-window)
    (with-current-buffer (egerrit-get-buffer "egerrit-conversations")
      ;; Update conversation
      (setq egerrit--current-entities
            (seq-map (lambda (it)
                       (if (string= id (egerrit--conversation-id it))
                           (progn
                             (setq updated t)
                             conversation)
                         it))
                     egerrit--current-entities))

      ;; Add new conversation
      (unless updated
        (push conversation egerrit--current-entities))

      ;; Refresh buffer content
      (egerrit--generate-comments-buffer egerrit--current-entities)
      (read-only-mode)
      (egerrit--goto-to-conversation (intern id))
      (when (window-live-p (get-buffer-window (current-buffer)))
        (recenter)
        (select-window (get-buffer-window (current-buffer)))))))

;;;###autoload
(defun egerrit-author-review-change (change)
  "Review CHANGE as an author."
  (interactive
   (list (tabulated-list-get-id)))
  (let ((egerrit--switch-branch nil)
        (egerrit--mode 'author))
    (egerrit-review-change change)))

;;;###autoload
(defun egerrit-author-review-partial-change (change)
  "Review a partial CHANGE as an author."
  (interactive
   (list (tabulated-list-get-id)))
  (let ((egerrit--switch-branch nil)
        (egerrit--mode 'author))
    (egerrit-review-partial-change change)))

;;;###autoload
(defun egerrit-review-change (change)
  "Review CHANGE."
  (interactive
   (list (tabulated-list-get-id)))
  (when (egerrit-change-p change)
    (let* ((egerrit--current-change (egerrit-get-detailed-change change))
           (revision
            (if current-prefix-arg
                (egerrit-select-revision egerrit--current-change)
              (seq-first (seq-reverse (egerrit--change-revisions egerrit--current-change))))))
      (if (and (eq egerrit--mode 'reviewer)
               (not (egerrit--clean-git-repository change)))
          (message "The git repository can't be dirty when engaging in a review")
        (egerrit--open-revision-diff revision egerrit--current-change)))))

;;;###autoload
(defun egerrit-review-partial-change (change)
  "Review a partial CHANGE."
  (interactive
   (list (tabulated-list-get-id)))
  (when (egerrit-change-p change)
    (let* ((egerrit--current-change (egerrit-get-detailed-change change))
           (old-revision
            (egerrit-select-revision egerrit--current-change
                                     (butlast
                                      (egerrit--change-revisions egerrit--current-change))))
           (new-revision
            (if current-prefix-arg
                (egerrit-select-revision egerrit--current-change
                                         (thread-last (egerrit--change-revisions egerrit--current-change)
                                                      (seq-filter (lambda (it) (> (egerrit--revision-number it)
                                                                             (egerrit--revision-number old-revision))))))
              (seq-first (seq-reverse (egerrit--change-revisions egerrit--current-change))))))
      (if (and (eq egerrit--mode 'reviewer)
               (not (egerrit--clean-git-repository change)))
          (message "The git repository can't be dirty when engaging in a review")
        (egerrit--open-partial-revision-diff old-revision new-revision egerrit--current-change)))))

;;;###autoload
(defun egerrit-publish-comments ()
  "Publish comments."
  (interactive)
  (let ((change egerrit--current-change)
        (revision egerrit--current-revision)
        (draft-comments
         (thread-last egerrit--current-entities
                      (seq-map #'egerrit--create-draft-comment)
                      (seq-remove #'null))))

    ;; Send draft comments
    (seq-map (lambda (it)
               (egerrit-put-draft-comment
                egerrit--current-change
                egerrit--current-revision
                it))
             draft-comments)

    ;; Publish draft comments
    (run-with-timer
     2 nil
     (lambda ()
       (egerrit-post-draft-comments change revision)))
    (egerrit-quit-review-or-feedback)))

;;;###autoload
(defun egerrit-focus-on-conversation ()
  "Put focus on current conversation."
  (interactive)
  (remove-overlays (point-min) (point-max))
  (when-let* ((start (previous-single-property-change (point) 'conversation))
              (ov (make-overlay (point-min) start)))
    (overlay-put ov 'face 'egerrit-context-highlight-out-of-focus-face))
  (when-let* ((end (next-single-property-change (point) 'conversation))
              (ov (make-overlay end (point-max))))
    (overlay-put ov 'face 'egerrit-context-highlight-out-of-focus-face)))

;;;###autoload
(defun egerrit-next-conversation ()
  "Navigate to the next conversation."
  (interactive)
  (if-let* ((conversation-end
             (save-excursion
               (prop-match-end (text-property-search-forward 'conversation t t))))
            (next-comments
             (save-excursion
               (goto-char conversation-end)
               (text-property-search-forward 'comments t t))))
      (progn
        (goto-char (prop-match-beginning next-comments))
        (recenter)
        (if (eq egerrit--conversation-mode 'review)
            (progn
              (egerrit-visit-conversation)
              (other-window 1))
          (when (egerrit-visit-conversation-source-code t)
            (other-window 1)))
        t)
    (message "No next comment")
    nil))

;;;###autoload
(defun egerrit-previous-conversation ()
  "Navigate to the previous conversation."
  (interactive)
  (if-let* ((conversation-begin
             (save-excursion
               (prop-match-beginning (text-property-search-backward 'conversation t t))))
            (previous-comments
             (save-excursion
               (goto-char conversation-begin)
               (text-property-search-backward 'comments t t))))
      (progn
        (goto-char (prop-match-beginning previous-comments))
        (recenter)
        (if (eq egerrit--conversation-mode 'review)
            (progn
              (egerrit-visit-conversation)
              (other-window 1))
          (when (egerrit-visit-conversation-source-code t)
            (other-window 1)))
        t)
    (message "No previous comment")
    nil))

;;;###autoload
(defun egerrit-visit-conversation ()
  "Visit conversation at point."
  (interactive)
  (let* ((conversation-id (symbol-name (plist-get (text-properties-at (point)) 'conversation-id)))
         (conversation (seq-find (lambda (it) (string= (egerrit--conversation-id it) conversation-id)) egerrit--current-entities)))
    (if (eq egerrit--conversation-mode 'feedback)
        (egerrit-visit-conversation-source-code)
      (select-window (get-buffer-window (egerrit-get-buffer "egerrit-revision-diff")))
      (goto-char (seq-elt (egerrit-region conversation) 0))
      (set-mark (point))
      (goto-char (seq-elt (egerrit--conversation-region conversation) 1))
      (activate-mark)
      (recenter-top-bottom))))

(defun egerrit-visit-conversation-source-code (&optional suppress-commit-message)
  "Visit conversation source code.

Optionally SUPPRESS-COMMIT-MESSAGE."
  (interactive)
  (let* ((conversation-id (plist-get (text-properties-at (point)) 'conversation-id))
         (conversation (seq-find (lambda (it) (string= (egerrit--conversation-id it) conversation-id)) egerrit--current-entities))
         (file (egerrit--conversation-file conversation))
         (range (egerrit--conversation-range conversation)))
    (if (string= "/COMMIT_MSG" file)
        (unless suppress-commit-message
          (funcall egerrit-commit-message-source-code-function)
          nil)
      (if (not (file-exists-p file))
          (progn
            (message "Source file %s doesn't exist" file)
            nil)
        (find-file-other-window file)
        (goto-char (point-min))
        (forward-line (1- (egerrit--range-start-line range)))
        (move-to-column (egerrit--range-start-character range))
        t))))

;;;###autoload
(defun egerrit-kill-comment ()
  "Kill comment at point."
  (interactive)
  (let* ((id (plist-get (text-properties-at (point)) 'conversation-id))
         (new-location)
         (conversation-deleted nil))
    ;; Update entities by removing comment
    (when id
      (setq egerrit--current-entities
            (thread-last egerrit--current-entities
                         (seq-map (lambda (it)
                                    (if (string= (symbol-name id) (egerrit--conversation-id it))
                                        (let ((comments (egerrit--conversation-comments it)))
                                          (if (= (length comments) 1)
                                              (progn
                                                (setq conversation-deleted t)
                                                nil)
                                            (setf (egerrit--conversation-comments it) (butlast comments))
                                            it))
                                      it)))
                         (seq-remove #'null)))

      ;; Determine location
      (if (not conversation-deleted)
          (setq new-location id)
        (let ((inhibit-message t))
          (setq new-location
                (cond ((egerrit-next-conversation) (plist-get (text-properties-at (point)) 'conversation-id))
                      ((egerrit-previous-conversation) (plist-get (text-properties-at (point)) 'conversation-id))
                      (t nil)))))

      (egerrit--generate-comments-buffer egerrit--current-entities)
      (read-only-mode)

      ;; Move to new location exist
      (when new-location
        (egerrit--goto-to-conversation new-location)
        (recenter)))))

(defun egerrit--goto-to-conversation (conversation-id)
  "Move to conversation with CONVERSATION-ID."
  (goto-char (point-min))
  (if-let* ((at-location (eq conversation-id (plist-get (text-properties-at (point)) 'conversation-id)))
            (comments-start
             (text-property-search-forward 'comments t t)))
      (goto-char (prop-match-beginning comments-start))
    (when-let* ((conversation-location
                 (text-property-search-forward 'conversation-id conversation-id (lambda (value current-value) (eq value current-value))))
                (comments-start
                 (progn
                   (goto-char (prop-match-beginning conversation-location))
                   (text-property-search-forward 'comments t t))))
      (goto-char (prop-match-beginning comments-start))))
  (egerrit-focus-on-conversation))

;;;###autoload
(defun egerrit-toggle-conversation-status ()
  "Toggle status on conversation at point."
  (interactive)
  (let* ((id (plist-get (text-properties-at (point)) 'conversation-id))
         (position (line-number-at-pos)))
    (when id
      (setq egerrit--current-entities
            (thread-last egerrit--current-entities
                         (seq-map (lambda (it)
                                    (when (string= (symbol-name id) (egerrit--conversation-id it))
                                      (let ((status (egerrit--conversation-status it)))
                                        (setf (egerrit--conversation-status it)
                                              (if (eq 'unresolved status)
                                                  'resolved
                                                'unresolved))))
                                    it))))
      (egerrit--generate-comments-buffer egerrit--current-entities)
      (goto-char (point-min))
      (forward-line (1- position))
      (egerrit-focus-on-conversation)
      (recenter))))

;;;###autoload
(defun egerrit-quit-review-or-feedback ()
  "Quit review or feedback."
  (interactive)
  (let ((default-directory
          (egerrit-get-project-root egerrit--current-change)))
    (egerrit-switch-to-previous-git-branch))
  (let ((buffers (pcase egerrit--conversation-mode
                   ('review `(,(egerrit-get-buffer "egerrit-revision-diff")
                              ,(egerrit-get-buffer "egerrit-conversations")))
                   ('feedback `(,(egerrit-get-buffer "egerrit-conversations"))))))
    (seq-map #'kill-buffer buffers)
    (delete-other-windows)
    (switch-to-buffer (egerrit-get-buffer "egerrit-dashboard"))))

;;;###autoload
(defun egerrit-quit-job ()
  "Quit job."
  (interactive)
  (egerrit-switch-to-previous-git-branch)
  (delete-other-windows)
  (switch-to-buffer egerrit--current-dashboard))

;;;###autoload
(defun egerrit-switch-to-other-job ()
  "Switch to another job from the current change and revision."
  (interactive)
  (let* ((egerrit--switch-branch nil)
         (change egerrit--current-change)
         (revision egerrit--current-revision)
         (jobs egerrit--current-jobs)
         (job (egerrit-select-job jobs))
         (buffer (egerrit-get-buffer "egerrit-job"))
         (git-branch egerrit--current-git-branch))
    (with-current-buffer buffer
      (egerrit--open-job job buffer)
      (egerrit--update-git-branch `(,buffer) git-branch)
      (setq egerrit--current-jobs jobs)
      (setq egerrit--current-change change)
      (setq egerrit--current-revision revision))))

;;;;; Job

;;;###autoload
(defun egerrit-author-open-job (change)
  "Open one of CHANGE's jobs."
  (interactive
   (list (tabulated-list-get-id)))
  (let ((egerrit--switch-branch nil)
        (egerrit--mode 'author))
    (egerrit-open-job change)))

;;;###autoload
(defun egerrit-open-job (change &optional revision)
  "Open one of CHANGE's jobs.

Optionally provide a REVISION to select job from."
  (interactive
   (list (tabulated-list-get-id)))
  (when (egerrit-change-p change)
    (if (and (eq egerrit--mode 'reviewer)
             (not (egerrit--clean-git-repository change)))
        (message "The git repository can't be dirty when engaging in a review")
      (when-let* ((egerrit--current-change (egerrit-get-detailed-change change))
                  (project-root (egerrit-get-project-root egerrit--current-change))
                  (default-directory project-root)
                  (revision
                   (or revision
                       (if current-prefix-arg
                           (egerrit-select-revision egerrit--current-change)
                         (seq-first (seq-reverse (egerrit--change-revisions egerrit--current-change))))))
                  (mode egerrit--mode)
                  (buffer (egerrit-get-buffer "egerrit-job"))
                  (jobs (egerrit--revision-jobs revision))
                  (job (egerrit-select-job jobs)))
        (with-current-buffer buffer
          (egerrit--open-job job buffer)
          (egerrit--switch-branch change revision)
          (setq egerrit--current-jobs jobs)
          (setq egerrit--current-change change)
          (setq egerrit--current-revision revision)
          (setq egerrit--current-dashboard (egerrit-get-buffer "egerrit-dashboard"))
          (setq egerrit--mode mode)
          (setq-local default-directory project-root))))))

;;;;; General

;;;###autoload
(defun egerrit-browse-url (entity)
  "Browse ENTITY's URL."
  (interactive
   (list (tabulated-list-get-id)))
  (browse-url
   (egerrit-url entity)))

;;;###autoload
(defun egerrit-yank-url ()
  "Yank URL."
  (interactive)
  (let ((entity
         (pcase major-mode
           ('egerrit-dashboard-mode (tabulated-list-get-id))
           ('egerrit-diff-mode egerrit--current-revision)
           ('egerrit-conversation-mode egerrit--current-revision)
           ('egerrit-job-mode egerrit--current-job)
           (_ nil))))
    (kill-new
     (egerrit-url entity))))

;;;; Support functions

;;;;; Constructors

(defun egerrit--create-change (change)
  "Return a `egerrit-change' created from CHANGE."
  (let-alist change
    (egerrit--change-create
     :id .id
     :changeid .change_id
     :number ._number
     :project .project
     :subject .subject
     :status .status
     :created .created
     :updated .updated
     :topic .topic
     :work-in-progress .work_in_progress
     :owner (egerrit--create-user .owner)
     :assignee (egerrit--create-user .assignee)
     :reviews (egerrit--create-reviews .labels)
     :unresolved-comments .unresolved_comment_count)))

(defun egerrit--create-detailed-change (change)
  "Return a detailed `egerrit-change' created from CHANGE."
  (let-alist change
    (let ((egerrit--current-change-number ._number)
          (egerrit--current-change-id .change_id)
          (egerrit--current-project .project))
      (egerrit--change-create
       :id .id
       :project egerrit--current-project
       :branch .branch
       :topic .topic
       :changeid egerrit--current-change-id
       :subject .subject
       :status .status
       :created .created
       :updated .updated
       :conversations (egerrit-get-conversations egerrit--current-change-id)
       :unresolved-comments .unresolved_comment_count
       :number egerrit--current-change-number
       :owner (egerrit--create-user .owner)
       :reviewers .reviewers
       :labels .labels
       :assignee .assignee
       :messages (egerrit--create-messages .messages)
       :work-in-progress .work_in_progress
       :current-revision .current_revision
       :revisions (egerrit--create-revisions .revisions)))))

(defun egerrit--create-comments (json-comments)
  "Return a list of `egerrit-comment' from JSON-COMMENTS."
  (pcase-let* ((`(,file . ,comments) json-comments))
    (seq-map (lambda (comment)
               (let-alist comment
                 (egerrit--comment-create
                  :id .id
                  :patch-set .patch_set
                  :file file
                  :author (egerrit--create-user .author)
                  :line .line
                  :range (if .range
                             (egerrit--create-range .range)
                           (egerrit--range-create :start-line .line
                                                  :end-line .line))
                  :context (egerrit--comment-highlight-context comment file)
                  :updated .updated
                  :message .message
                  :in-reply-to .in_reply_to
                  :side (if .side 'parent 'revision)
                  :unresolved .unresolved)))
             comments)))

(defun egerrit--comment-highlight-context (comment file)
  "Extract and return the context of the COMMENT made on FILE."
  (let* ((context-lines (alist-get 'context_lines comment))
         (context  (string-join
                    (seq-map (lambda (it) (alist-get 'context_line it)) context-lines)
                    "\n"))
         (line-offset)
         (start))
    (unless (string-empty-p context)
      (setq line-offset (- (let-alist comment .range.start_line)
                           (let-alist (seq-first context-lines) .line_number)))
      (with-temp-buffer
        (insert context)
        (goto-char (point-min))
        (forward-line line-offset)
        (move-to-column (let-alist comment .range.start_character))
        (setq start (point))
        (goto-char (point-max))
        (forward-line (- 0 line-offset))
        (move-to-column (let-alist comment .range.end_character))
        (when-let* ((no-commit-message (not (eq '/COMMIT_MSG file)))
                    (extension (file-name-extension (symbol-name file) t))
                    (mode (thread-last auto-mode-alist
                                       (seq-find (lambda (it)
                                                   (string-match-p (car it) extension)))
                                       (cdr))))
          (funcall mode)
          ;; We ignore errors since the context is not guaranteed to be correct
          (ignore-errors
            (font-lock-ensure)))
        ;; Add text properties to the whole region
        (remove-text-properties (point-min) (point-max) '(font-lock-face fontified))
        (font-lock-append-text-property (point-min) (point-max) 'face 'egerrit-context-background-face)

        ;; Add a text property to highlight region
        (add-text-properties start (point) '(face egerrit-context-highlight-face))
        (buffer-string)))))

(defun egerrit--create-download (download)
  "Convert DOWNLOAD into a `egerrit-download' object."
  (let-alist download
    (egerrit--download-create
     :url .url
     :ref .ref
     :checkout .commands.Checkout
     :cherry-pick .commands.Cherry\ Pick
     :format-patch .commands.Format\ Patch
     :cherry-pick .commands.Cherry\ Pick
     :pull .commands.Pull)))

(defun egerrit--create-fetch (fetch)
  "Convert FETCH into a `egerrit-fetch' object."
  (let-alist fetch
    (egerrit--fetch-create
     :ssh (egerrit--create-download .ssh)
     :http (egerrit--create-download .http))))

(defun egerrit--create-reviews (reviews)
  "Return a list of `egerrit-review' objects created from REVIEWS."
  (let ((review-types '(Code-Review Secondary-Review Verified)))
    (seq-map (lambda (type)
               (let* ((review (alist-get type reviews))
                      (value (caar review))
                      (info (alist-get value review)))
                 (egerrit--review-create
                  :type type
                  :value value
                  :reviewer (egerrit--create-user info))))
             review-types)))

(defun egerrit--create-messages (messages)
  "Return a list `egerrit-message' objects created from MESSAGES."
  (seq-map (lambda (message)
             (let-alist message
               (egerrit--message-create
                :id .id
                :author (egerrit--create-user .author)
                :date .date
                :message .message
                :number ._revision_number)))
           messages))

(defun egerrit--create-user (user)
  "Convert USER into a `egerrit-user' object."
  (let-alist user
    (egerrit--user-create
     :name .name
     :username .username
     :email .email)))

(defun egerrit--create-revisions (revisions)
  "Return a list of `egerrit-revision' objects created from REVISIONS."
  (thread-last revisions
               (seq-map (lambda (it)
                          (cl-destructuring-bind (id . revision) it
                            (let-alist revision
                              (let ((egerrit--current-revision-id id)
                                    (egerrit--current-revision-number ._number))
                                (egerrit--revision-create
                                 :id egerrit--current-revision-id
                                 :number egerrit--current-revision-number
                                 :created .created
                                 :uploader (egerrit--create-user .uploader)
                                 :ref .ref
                                 :fetch (egerrit--create-fetch .fetch)
                                 :commit (egerrit--create-commit .commit)))))))
               (seq-sort-by #'egerrit--revision-number #'<)))

(defun egerrit--create-jobs (message regexp)
  "Return a list of `egerrit-job' objects matching REGEXP in MESSAGE."
  ;; TODO: Consider creating a concept of instances. All jobs
  ;; belonging to a message belongs to one instance.
  (let ((revision (egerrit--message-number message))
        (lines (split-string (egerrit--message-message message) "\n" t)))
    (thread-last lines
                 (seq-map (lambda (line)
                            (when (string-match regexp line)
                              (egerrit--job-create
                               :name (match-string 1 line)
                               :url (match-string 2 line)
                               :status (downcase (match-string 3 line))
                               :time (match-string 4 line)
                               :revision revision
                               :date (egerrit--message-date message)))))
                 (seq-remove #'null))))

(defun egerrit--create-commit (commit)
  "Return an `egerrit-commit' object created from COMMIT."
  (let-alist commit
    (egerrit--commit-create
     :parents .parents
     :author (egerrit--create-user .author)
     :committer (egerrit--create-user .comitter)
     :subject .subject
     :message .message)))

(defun egerrit--create-range (range)
  "Return a `egerrit-range' object based on RANGE."
  (let-alist range
    (egerrit--range-create
     :start-line .start_line
     :start-character .start_character
     :end-line .end_line
     :end-character .end_character)))

;;;;; Gerrit/Communication

(defun egerrit--post-message (message change)
  "Send MESSAGE to CHANGE."
  (let* ((uri (format "/changes/%s/revisions/%s/review"
                      (egerrit--change-id change)
                      (egerrit--change-current-revision change)))
         (url (format "https://%s/a%s" egerrit-request-url uri))
         (data `(("message" . ,message))))
    (egerrit-request
     url
     :type "POST"
     :header '(("Content-Type" . "application/json"))
     :auth t
     :data data)))

(defun egerrit--post-reviewer (reviewer change)
  "Add REVIEWER to CHANGE."
  (let* ((id (egerrit--change-id change))
         (url (concat "https://" egerrit-request-url "/a/changes/" id "/reviewers"))
         (data `(("reviewer" . ,reviewer))))
    (egerrit-request
     url
     :type "POST"
     :header '(("Content-Type" . "application/json"))
     :auth t
     :data data)))

(defun egerrit--post-cc (reviewer change)
  "Add REVIEWER to CHANGE."
  (let* ((id (egerrit--change-id change))
         (url (concat "https://" egerrit-request-url "/a/changes/" id "/reviewers"))
         (data `(("reviewer" . ,reviewer)
                 ("state" . ,"CC"))))
    (egerrit-request
     url
     :type "POST"
     :header '(("Content-Type" . "application/json"))
     :auth t
     :data data)))

(defun egerrit--read-users (max-users start-offset &optional name-contain)
  "Query Gerrit for all users with MAX-USERS and START-OFFSET.

Optionally provide a NAME-CONTAIN variable to narrow the search space."
  (let ((url (concat "https://" egerrit-request-url "/a/accounts/"))
        (params `(("q" . ,(if name-contain
                              (concat "is:active AND name:" name-contain)
                            "is:active"))
                  ("o" . "DETAILS")
                  ("n" . ,(number-to-string max-users))
                  ("start" . ,(number-to-string start-offset)))))
    (egerrit-request
     url
     :auth t
     :type "GET"
     :header '(("Content-Type" . "application/json"))
     :params params
     :parser #'egerrit-request-parser)))

(defun egerrit--read-changes (query &optional max-changes)
  "Send QUERY to Gerrit for change(s) with MAX-CHANGES."
  (let* ((max-changes (or max-changes egerrit-max-changes))
         (url (concat "https://" egerrit-request-url "/a/changes/"))
         (params `(("q" . ,query)
                   ("n" . ,max-changes)
                   ("o" . "LABELS")
                   ("o" . "DETAILED_ACCOUNTS"))))
    (egerrit-request
     url
     :auth t
     :type "GET"
     :header '(("Content-Type" . "application/json"))
     :params params
     :parser #'egerrit-request-parser)))

(defun egerrit--read-change (id)
  "Query Gerrit for ID."
  (let ((url (concat "https://" egerrit-request-url "/a/changes/"))
        (params `(("q" . ,id)
                  ("o" . "DETAILED_LABELS")
                  ("o" . "ALL_REVISIONS")
                  ("o" . "ALL_COMMITS")
                  ("o" . "DETAILED_ACCOUNTS")
                  ("o" . "MESSAGES")
                  ("o" . "CHANGE_ACTIONS")
                  ("o" . "REVIEWER_UPDATES")
                  ("o" . "DOWNLOAD_COMMANDS"))))
    (egerrit-request
     url
     :auth t
     :type "GET"
     :header '(("Content-Type" . "application/json"))
     :params params
     :parser #'egerrit-request-parser)))

(defun egerrit--read-comments (changeid)
  "Query Gerrit for comments on CHANGEID."
  (let ((url (concat "https://" egerrit-request-url "/a/changes/" changeid "/comments")))
    (egerrit-request
     url
     :auth t
     :type "GET"
     :header '(("Content-Type" . "application/json"))
     :params `(("enable-context" . "true")
               ("context-padding" . ,egerrit-gerrit-conversation-context))
     :parser #'egerrit-request-parser)))

(defun egerrit--read-change-dependencies (change)
  "Return a list of dependencies for CHANGE."
  (let* ((changeid (egerrit--change-id change))
         (revisionid (egerrit--change-current-revision change))
         (url (concat "https://" egerrit-request-url "/a/changes/" changeid "/revisions/" revisionid "/related")))
    (egerrit-request
     url
     :type "GET"
     :header '(("Content-Type" . "application/json"))
     :auth t)))

;;;;; Diff

(defun egerrit--diff-locate-conversation-hunk (conversation)
  "Locate the hunk that the CONVERSATION and return data."
  (let ((side (egerrit--conversation-side conversation))
        (commit-message-hunk (string= "/COMMIT_MSG" (egerrit--conversation-file conversation)))
        (gerrit-commit-message-offset 7))
    (goto-char (point-min))
    (if commit-message-hunk
        (progn
          (search-forward "Subject: [PATCH] ")
          `(,gerrit-commit-message-offset
            ,(save-excursion (diff-hunk-next) (line-number-at-pos))))

      ;; 1) Find the boundaries of the file of interest
      (let ((file-regexp
             (if (eq side 'revision)
                 (format "+++ b/%s" (egerrit--conversation-file conversation))
               (format "--- a/%s" (egerrit--conversation-file conversation)))))

        ;; Conversations from a previous patch set are not always found in the diff
        (if (search-forward-regexp file-regexp nil t)
          (pcase-let ((`(,file-start ,file-end) (diff-bounds-of-file))
                      (location)
                      (conversation-line (egerrit--range-start-line
                                          (egerrit--conversation-range conversation))))
            ;; 2) Find the hunk that best matches the range described in
            ;; the conversation
            (goto-char (1- file-end))
            (setq location `(,(egerrit--diff-hunk-line-offset side)
                             ,(save-excursion
                                (goto-char (1- (diff-end-of-hunk)))
                                (line-number-at-pos))))

            (save-excursion
              (while (and (<= file-start (point))
                          (<= conversation-line (egerrit--diff-hunk-line-offset side))
                          (not (eobp)))
                (diff-hunk-prev)
                (goto-char (1- (point)))
                (setq location `(,(egerrit--diff-hunk-line-offset side)
                                 ,(line-number-at-pos)))))

            ;; 3) Return the location info
            (diff-beginning-of-hunk)
            location)
          ;; File can not be found
          '(0 0))))))

(defun egerrit--diff-locate-conversation (conversation)
  "Locate a CONVERSATION in the diff buffer."
  (let* ((side (egerrit--conversation-side conversation))
         (range (egerrit--conversation-range conversation))
         ;; TODO(Niklas Eklund, 20220405): Ideally file-level-comment
         ;; should locate the correct file, but now we will end up at
         ;; the top of the diff buffer. This is something to improve
         ;; in the future
         (file-level-comment (null (egerrit--range-start-line range)))
         (commit-message-hunk (string= "/COMMIT_MSG" (egerrit--conversation-file conversation)))
         (patchset-level (string= "/PATCHSET_LEVEL" (egerrit--conversation-file conversation)))
         (found-it))
    (save-excursion
      (if (or patchset-level file-level-comment)
          `(0 0)
        (if-let ((location (egerrit--diff-locate-conversation-hunk conversation)))
            ;; Move to the correct line
            (save-excursion
              (pcase-let ((`(,line ,hunk-end-line) location))
                (while
                    (and
                     (< (line-number-at-pos) hunk-end-line)
                     (not found-it)
                     (not (eobp)))
                  (forward-line 1)
                  (unless (string-prefix-p (if (eq side 'revision) "-" "+") (thing-at-point 'line))
                    (setq line (1+ line)))
                  (when (= line (egerrit--range-start-line range))
                    (setq found-it t)))
                ;; Move to the correct column if comment is found inside the hunk
                (when found-it
                  (if-let ((start-char (egerrit--range-start-character range)))
                      (move-to-column (+ (if commit-message-hunk 0 1)
                                         start-char))
                    (beginning-of-line))))
              ;; Return point as region
              `(,(point) ,(point)))
          `(0 0))))))

(defun egerrit--diff-commit-message-boundary ()
  "Return the line number for the commit message boundary."
  (let ((hunk (buffer-substring-no-properties
               (point-min)
               (save-excursion (diff-beginning-of-hunk) (point)))))
    (with-temp-buffer
      (insert hunk)
      (goto-char (point-min))
      (search-forward "Subject: [PATCH] ")
      (line-number-at-pos))))

(defun egerrit--diff-hunk (side)
  "Return a refined HUNK based on SIDE."
  (let ((hunk (buffer-substring-no-properties
               (save-excursion (diff-beginning-of-hunk) (point))
               (save-excursion (diff-end-of-hunk) (point)))))
    (string-join
     (thread-last (split-string hunk "\n" t)
                  (seq-remove (lambda (it)
                                ;; Inverse, if parent remove all lines from revision
                                (string-prefix-p (if (eq side 'parent)
                                                     "+" "-")
                                                 it)))
                  (seq-map (lambda (it) (substring it 1 (length it))))
                  (seq-rest))
     "\n")))

(defun egerrit--diff-context (commit-message)
  "Return the context, special handling for COMMIT-MESSAGE."
  (let ((highlight (if (use-region-p)
                       (buffer-substring-no-properties
                        (region-beginning) (region-end))
                     (thing-at-point 'line))))
    (if commit-message
        highlight
      (string-join
       (seq-map (lambda (it) (substring it 1 (length it)))
                (split-string highlight "\n" t))
       "\n"))))

(defun egerrit--diff-side ()
  "Return the side at point."
  (let* ((revision-identifier "+")
         (parent-identifier "-")
         (hunk (buffer-substring-no-properties
                (save-excursion (beginning-of-line) (point))
                (save-excursion (diff-end-of-hunk) (point))))
         (revision-location)
         (parent-location))
    (with-temp-buffer
      (insert hunk)
      (goto-char (point-min))
      (save-excursion
        (setq revision-location
              (search-forward-regexp (rx-to-string `(and bol ,revision-identifier)) nil t)))
      (save-excursion
        (setq parent-location
              (search-forward-regexp (rx-to-string `(and bol ,parent-identifier)) nil t))))
    (cond ((and revision-location parent-location) (if (< revision-location parent-location)
                                                       'revision
                                                     'parent))
          ((not (null revision-location)) 'revision)
          ((not (null parent-location)) 'parent)
          (t 'parent))))

(defun egerrit--diff-hunk-line-offset (side)
  "Return the line-offset for the current hunk, use SIDE."
  (let ((hunk-header
         (save-excursion (diff-beginning-of-hunk)
                         (thing-at-point 'line)))
        (re (rx-to-string `(and ,(if (eq side 'parent) "-" "+") (group (one-or-more digit)) ","))))
    (string-match re hunk-header)
    (1- (string-to-number (match-string 1 hunk-header)))))

(defun egerrit--diff-range (side boundary line-offset column-offset)
  "Return a range for SIDE considering BOUNDARY, LINE-OFFSET and COLUMN-OFFSET."
  (let ((begin (region-beginning))
        (begin-line 0)
        (end-line 0)
        (end (region-end)))

    ;; Start
    (save-excursion
      (goto-char begin)
      (while (< boundary (line-number-at-pos))
        (forward-line -1)
        (unless (string-prefix-p (if (eq side 'revision) "-" "+") (thing-at-point 'line))
          (setq begin-line (1+ begin-line)))))

    ;; end
    (save-excursion
      (goto-char end)
      (while (< boundary (line-number-at-pos))
        (forward-line -1)
        (unless (string-prefix-p (if (eq side 'revision) "-" "+") (thing-at-point 'line))
          (setq end-line (1+ end-line)))))

    (egerrit--range-create
     :start-line (+ line-offset begin-line)
     :start-character (save-excursion (goto-char begin)
                                      (max 0 (+ column-offset (current-column))))
     :end-line (+ line-offset end-line)
     :end-character (save-excursion (goto-char end)
                                    (max 0 (+ column-offset (current-column)))))))

(defun egerrit--diff-review-comment ()
  "Return a comment.
This comment is based on highlight in diff buffer."
  (let* ((side 'revision)
         (range)
         (region (if (use-region-p)
                     `(,(region-beginning) ,(region-end))
                   `(,(save-excursion (beginning-of-line) (point))
                     ,(save-excursion (end-of-line) (point)))))
         (gerrit-commit-message-offset 7)
         (commit-message-hunk (< (point)
                                 (save-excursion (diff-beginning-of-hunk) (point))))
         (file))
    (if commit-message-hunk
        ;; Comment on a commit message
        (progn
          (setq file "/COMMIT_MSG")
          (setq side 'revision)
          (setq range (egerrit--diff-range side
                                           (egerrit--diff-commit-message-boundary)
                                           gerrit-commit-message-offset
                                           0)))
      ;; Comment on a diff hunk
      (setq side (egerrit--diff-side))
      (setq file (if (eq side 'revision)
                     (string-remove-prefix "b/" (seq-elt (diff-hunk-file-names) 0))
                   (string-remove-prefix "a/" (seq-elt (diff-hunk-file-names) 1))))
      (setq range (egerrit--diff-range side
                                       (save-excursion
                                         (diff-beginning-of-hunk)
                                         (line-number-at-pos))
                                       (egerrit--diff-hunk-line-offset side)
                                       -1)))
    (egerrit--conversation-create
     :id (secure-hash 'md5 (current-time-string))
     :file file
     :patch-set (egerrit--revision-number egerrit--current-revision)
     :side side
     :range range
     :status 'unresolved
     :comments nil
     :region region)))

;;;;; Conversations

(defun egerrit--create-conversations (file-conversations)
  "Create `egerrit-conversation' objects from FILE-CONVERSATIONS."
  (pcase-let* ((`(,file-name . ,conversations) file-conversations))
    (seq-map (lambda (it)
               (let ((first-comment (seq-first it))
                     (last-comment (seq-first (seq-reverse it))))
                 (let-alist first-comment
                   (egerrit--conversation-create
                    :id (alist-get 'id last-comment)
                    :file (symbol-name file-name)
                    :context-lines .context_lines
                    :comments (egerrit--create-conversation-comments it)
                    :patch-set .patch_set
                    :range (if .range
                               (egerrit--create-range .range)
                             (egerrit--range-create :start-line .line
                                                    :end-line .line))
                    :side (if .side 'parent 'revision)
                    :status (if (alist-get 'unresolved last-comment) 'unresolved 'resolved)))))
             conversations)))

(defun egerrit--get-conversations (change revision)
  "Return comments from CHANGE based on REVISION.

Return comments that are unresolved and are either created on revision
or previous revisions."
  (let* ((revision-number (egerrit--revision-number revision))
         (conversations
          (thread-last (egerrit--change-conversations change)
                       (seq-remove (lambda (it) (> (egerrit--conversation-patch-set it) revision-number)))
                       (seq-filter (lambda (it) (eq 'unresolved (egerrit--conversation-status it))))
                       (seq-map #'egerrit--generate-conversation-context))))
    conversations))

(defun egerrit--generate-conversation-context (conversation)
  "Update CONVERSATION with a context."
  (if (egerrit--conversation-context conversation)
      ;; No need to regenerate context
      conversation
    (if (not (egerrit--conversation-context-lines conversation))
        ;; No context-lines, no context
        nil
      (let* ((file (egerrit--conversation-file conversation))
             (range (egerrit--conversation-range conversation))
             (context-lines (egerrit--conversation-context-lines conversation))
             (context  (string-join
                        (seq-map (lambda (it) (alist-get 'context_line it)) context-lines)
                        "\n"))
             (start-line-offset)
             (end-line-offset)
             (start)
             (conversation-context))
        (if (string-empty-p context)
            conversation
          (setq start-line-offset (- (egerrit--range-start-line range)
                                     (let-alist (seq-first context-lines) .line_number)))
          (with-temp-buffer
            (insert context)
            (goto-char (point-min))
            (forward-line start-line-offset)
            (if-let ((start-char (egerrit--range-start-character range)))
                (move-to-column start-char)
              (beginning-of-line))
            (setq start (point))
            (setq end-line-offset (- (egerrit--range-end-line range)
                                     (let-alist (seq-first (seq-reverse context-lines)) .line_number)))
            (goto-char (point-max))
            (forward-line end-line-offset)
            (if-let ((end-char (egerrit--range-end-character range)))
                (move-to-column end-char)
              (end-of-line))
            (when-let* ((no-commit-message (not (string= "/COMMIT_MSG" file)))
                        (extension (file-name-extension file t))
                        (mode (thread-last auto-mode-alist
                                           (seq-find (lambda (it)
                                                       (string-match-p (car it) extension)))
                                           (cdr))))
              (funcall mode)
              ;; We ignore errors since the context is not guaranteed to be complete
              (ignore-errors
                (font-lock-ensure)))
            ;; Add text properties to the whole region
            (remove-text-properties (point-min) (point-max) '(font-lock-face fontified))
            (font-lock-append-text-property (point-min) (point-max) 'face 'egerrit-context-background-face)

            ;; Add a text property to highlight region
            (add-text-properties start (point) '(face egerrit-context-highlight-face))

            (setq conversation-context (buffer-string))
            (setf (egerrit--conversation-context conversation) conversation-context)
            conversation))))))

(defun egerrit--create-conversation-comments (json-comments)
  "Convert the list of JSON-COMMENTS to an alist."
  (seq-map
   (lambda (it)
     (let-alist it
       `(,.author.name . ,.message)))
   json-comments))

(defun egerrit--remove-blocked-comments (file-comments)
  "Remove blocked comments from FILE-COMMENTS."
  (pcase-let* ((`(,file-name . ,comments) file-comments)
               (filtered-comments))

    ;; Filter out blocked comments
    (setq filtered-comments
          (seq-remove (lambda (it)
                        (seq-find (lambda (message-regexp)
                                    (string-match message-regexp (alist-get 'message it)))
                                  egerrit-comment-block-list))
                      comments))
    `(,file-name . ,filtered-comments)))

(defun egerrit--group-conversation-comments (file-comments)
  "Group FILE-COMMENTS."
  (pcase-let* ((`(,file-name . ,comments) file-comments)
               ;; Find all comments that start conversations
               (conversation-starters
                (seq-remove (lambda (it)
                              (alist-get 'in_reply_to it))
                            comments))
               (conversations)
               (clustered-comments))

    ;; For each conversation starter find the related comments
    (setq clustered-comments
          (seq-map (lambda (it)
                     (egerrit--cluster-comments it comments))
                   conversation-starters))

    (setq conversations
          (thread-last clustered-comments
                       (nreverse)
                       (vconcat)))

    `(,file-name . ,conversations)))

(defun egerrit--cluster-comments (start-comment comments)
  "Cluster all COMMENTS which relates to START-COMMENT."
  (let* ((conversation-comments `(,start-comment))
         (id (alist-get 'id start-comment))
         (replies
          (seq-filter (lambda (it)
                        (string= (alist-get 'in_reply_to it) id))
                      comments)))

    ;; Iterate all comments until there is no reply
    (dolist (reply replies) conversation-comments
            (setq conversation-comments
                  `(,@conversation-comments
                    ,@(egerrit--cluster-comments reply comments))))
    conversation-comments))

(defun egerrit--create-draft-comment (conversation)
  "From a CONVERSATION create a draft comment."
  (let* ((comments (egerrit--conversation-comments conversation))
         (has-reply (string= (car (seq-first (seq-reverse comments))) "Reply"))
         (is-resolved (eq 'resolved (egerrit--conversation-status conversation)))
         (is-new-conversation (and has-reply (eq (length comments) 1))))
    ;; We want to cover these cases
    ;; 1) User initiated a new conversation
    ;; 2) User engaged in an existing conversation
    ;; 3) User changed the status of an existing conversation, automatically compose message with done
    (cond ((eq is-new-conversation t)
           (let ((range (egerrit--conversation-range conversation)))
             `(("path" . ,(egerrit--conversation-file conversation))
               ("unresolved" . "true")
               ("message" . ,(cdr (seq-first comments)))
               ("range" . (("start_line" . ,(egerrit--range-start-line range))
                           ("start_character" . ,(egerrit--range-start-character range))
                           ("end_line" . ,(egerrit--range-end-line range))
                           ("end_character" . ,(egerrit--range-end-character range)))))))
          ((eq has-reply t)
           `(("in_reply_to" . ,(egerrit--conversation-id conversation))
             ("path" . ,(egerrit--conversation-file conversation))
             ("unresolved" . ,(if is-resolved "false" "true"))
             ("message" . ,(cdr (seq-first (seq-reverse comments))))))
          ((eq is-resolved t)
           `(("in_reply_to" . ,(egerrit--conversation-id conversation))
             ("path" . ,(egerrit--conversation-file conversation))
             ("unresolved" . ,(if is-resolved "false" "true"))
             ("message" . ,"DONE")))
          (t nil))))

;;;;; String representations

(defun egerrit--pad-string (string length padding &optional start)
  "Pad STRING up to LENGTH with PADDING optionally START."
  (let ((pad-length (- length (length string))))
    (if (< pad-length 0)
        string
      (concat
       (and start
            (make-string pad-length padding))
       string
       (and (not start)
            (make-string pad-length padding))))))

(defun egerrit--generate-comments-str (conversation)
  "Generate a string representation of CONVERSATION's comments."
  (let ((comments-str
         (string-join
          (seq-map (lambda (it)
                     (let ((fill-column 100))
                       (with-temp-buffer
                         (insert (format "%s:\n %s\n" (car it) (cdr it)))
                         (funcall egerrit-comment-formatter)
                         (buffer-string))))
                   (egerrit--conversation-comments conversation))
          "")))
    (put-text-property 0 (length comments-str) 'comments t comments-str)
    comments-str))

(defun egerrit--generate-status-str (conversation)
  "Generate a string representation of CONVERSATION's status."
  (let ((status-value-str
         (if (eq 'unresolved (egerrit--conversation-status conversation))
             "TODO"
           "DONE"))
        (status-property-str "Status"))
    (setq status-value-str
          (with-temp-buffer
            (insert status-value-str)
            (funcall egerrit-status-formatter)
            (buffer-string)))
    (put-text-property 0 (length status-property-str) 'face 'egerrit-unknown-face status-property-str)
    (format "%s: %s\n" status-property-str status-value-str)))

(defun egerrit--conversation-patch-set-str (conversation)
  "Generate a string representation of CONVERSATION's patch-set."
  (let ((patch-set-str (format "Patch-set: %s\n" (egerrit--conversation-patch-set conversation))))
    (put-text-property 0 (length patch-set-str) 'face 'egerrit-unknown-face patch-set-str)
    patch-set-str))

(defun egerrit--conversation-file-str (conversation)
  "Generate a string representation of CONVERSATION's file."
  (let ((file-name (egerrit--conversation-file conversation))
        (side-str
         (if-let ((side (egerrit--conversation-side conversation)))
             (if (eq 'parent side)
                 "--- "
               "+++ ")
           "??? "))
        (file-str))
    (setq file-str
          (format "%s%s\n" side-str file-name))
    (put-text-property 0 (length file-str) 'face 'egerrit-file-name-face file-str)
    file-str))

(defun egerrit--conversation-str (conversation)
  "Return a string representation of CONVERSATION."
  (let* ((id (intern (egerrit--conversation-id conversation)))
         (file-str (egerrit--conversation-file-str conversation))
         (status-str (egerrit--generate-status-str conversation))
         (comments-str (egerrit--generate-comments-str conversation))
         (patch-set-str (egerrit--conversation-patch-set-str conversation))
         (context-start-line "src\n")
         (context-end-line "src\n")
         (context-str)
         (conversation-str))

    (put-text-property 0 (length context-start-line) 'face 'egerrit-context-block-begin-face context-start-line)
    (put-text-property 0 (length context-end-line) 'face 'egerrit-context-block-end-face context-end-line)

    (setq context-str
          (when-let ((context (egerrit--conversation-context conversation)))
            (format "%s%s\n%s\n"
                    context-start-line
                    context
                    context-end-line)))

    (setq conversation-str
          (string-join
           (seq-remove #'null
                       `(,file-str
                         ,status-str
                         ,patch-set-str
                         "\n"
                         ,context-str
                         ,comments-str))
           ""))

    (put-text-property 0 (length conversation-str) 'conversation-id id conversation-str)
    (put-text-property 0 (length conversation-str) 'conversation t conversation-str)

    conversation-str))

(defun egerrit--conversations-str (conversations)
  "Return a string representation of CONVERSATIONS."
  (string-join
   (thread-last conversations
                (seq-map
                 (lambda (it)
                   `(,(egerrit--conversation-str it) ; conversation
                     ,(format "") ; line-break
                     )))
                (flatten-list)
                (butlast))
   "\n"))

(defun egerrit--revision-jobs-str (revision)
  "Return a propertized string representation of jobs on REVISION."
  (if-let* ((jobs (egerrit--revision-jobs revision))
            (success (egerrit--job-count-status-str jobs "success" 'egerrit-success-face))
            (failure (egerrit--job-count-status-str jobs "failure" 'egerrit-error-face)))
      (format "%s - %s" failure success)
    ""))

(defun egerrit--job-count-status-str (jobs status face)
  "Return the count of STATUS in JOBS and apply FACE."
  (let ((str
         (thread-last jobs
                      (seq-filter (lambda (it) (string= status (egerrit--job-status it))))
                      (length)
                      (number-to-string))))
    (put-text-property 0 (length str) 'face face str)
    str))

(defun egerrit--review-str (change type)
  "Return a string representation of review of TYPE in CHANGE."
  (let* ((review (egerrit-review change type))
         (str (egerrit--map-review-value review)))
    (pcase review
      ('approved (put-text-property 0 (length str) 'face 'egerrit-success-face str))
      ('recommended (put-text-property 0 (length str) 'face 'egerrit-success-face str))
      ('rejected (put-text-property 0 (length str) 'face 'egerrit-error-face str))
      ('disliked (put-text-property 0 (length str) 'face 'egerrit-error-face str))
      (_ (put-text-property 0 (length str) 'face 'egerrit-no-review-face str)))
    str))

(defun egerrit--code-review-str (change)
  "Return a string representation CHANGE's code review."
  (egerrit--review-str change 'Code-Review))

(defun egerrit--secondary-review-str (change)
  "Return a string representation CHANGE's secondary review."
  (egerrit--review-str change 'Secondary-Review))

(defun egerrit--verified-review-str (change)
  "Return a string representation CHANGE's verified review."
  (egerrit--review-str change 'Verified))

(defun egerrit--subject-str (change)
  "Return the subject of CHANGE."
  (egerrit-subject change))

(defun egerrit--owner-str (change)
  "Return a propertized user based on CHANGE."
  (let ((owner (egerrit-owner change)))
    (put-text-property 0 (length owner) 'face 'egerrit-user-face owner)
    owner))

(defun egerrit--assignee-str (change)
  "Return a propertized assignee based on CHANGE."
  (let ((assignee (or (egerrit--user-name
                       (egerrit--change-assignee change))
                      "")))
    (put-text-property 0 (length assignee) 'face 'egerrit-user-face assignee)
    assignee))

(defun egerrit--project-str (change)
  "Return a propertized project name based on CHANGE."
  (let ((project (egerrit-project change)))
    (put-text-property 0 (length project) 'face 'egerrit-project-face project)
    project))

(defun egerrit--topic-str (change)
  "Return a propertized topic based on CHANGE."
  (let ((topic (egerrit--change-topic change)))
    (if (null topic)
        ""
      (put-text-property 0 (length topic) 'face 'egerrit-topic-face topic)
      topic)))

(defun egerrit--duration-str (job)
  "Return a propertized duration based on JOB."
  (let* ((updated (egerrit--job-time job))
         (str (car (split-string updated "\\."))))
    (put-text-property 0 (length str) 'face 'egerrit-date-face str)
    str))

(defun egerrit--updated-str (entity)
  "Return a propertized updated based on ENTITY."
  (let* ((updated-time (parse-time-string (egerrit-updated entity)))
         (str
          (if (> (days-between (current-time-string) (egerrit-updated entity))
                 0)
              (format "%s %s"
                      (capitalize (car (rassoc (decoded-time-month updated-time) parse-time-months)))
                      (egerrit--pad-string (number-to-string (decoded-time-day updated-time)) 2 48 t))
            (format "%s:%s" (egerrit--pad-string (number-to-string (decoded-time-hour updated-time)) 2 48 t)
                    (egerrit--pad-string (number-to-string (decoded-time-minute updated-time)) 2 48 t)))))
    (put-text-property 0 (length str) 'face 'egerrit-date-face str)
    str))

(defun egerrit--revision-unresolved-str (revision)
  "Return number of unresolved comments on REVISION."
  (number-to-string
   (thread-last (egerrit--revision-comments revision)
                (seq-filter #'egerrit--comment-unresolved)
                (length))))

(defun egerrit--revision-number-str (revision)
  "Return REVISION's number."
  (number-to-string (egerrit--revision-number revision)))

(defun egerrit--revision-comment-authors (revision)
  "Return a string containing comment authors on REVISION."
  (let* ((number (egerrit--revision-number revision))
         (conversations (egerrit--change-conversations egerrit--current-change))
         (comment-authors
          (thread-last conversations
                       (seq-filter (lambda (it)
                                     (= (egerrit--conversation-patch-set it) number)
                                     ))
                       (seq-map (lambda (it)
                                  (thread-last (egerrit--conversation-comments it)
                                               (seq-map #'car)
                                               (seq-uniq))

                                  ))
                       (flatten-list)
                       (seq-uniq))))
    comment-authors))

(defun egerrit--revision-comment-authors-str (revision)
  "Return a string of all comment authors on REVISION."
  (if-let ((authors (egerrit--revision-comment-authors revision)))
      (string-join
       authors
       ", ")
    ""))

(defun egerrit--job-status-str (job)
  "Return a propertized string of JOB's status."
  (let ((result (egerrit--job-status job)))
    (pcase result
      ("failure" (put-text-property 0 (length result) 'face 'egerrit-error-face result))
      ("fast fail" (put-text-property 0 (length result) 'face 'egerrit-error-face result))
      ("success" (put-text-property 0 (length result) 'face 'egerrit-success-face result))
      ("prev-success" (put-text-property 0 (length result) 'face 'egerrit-unknown-face result)))
    result))

(defun egerrit--change-status-str (change)
  "Return a propertized string of CHANGE's status."
  (let ((status '())
        (status-str))
    (when-let* ((comments (egerrit--change-unresolved-comments change))
                (has-comments (> comments 0)))
      (push egerrit-change-comments-status status))
    (when (egerrit--change-work-in-progress change)
      (push egerrit-change-wip-status status))
    (setq status-str (string-join (seq-remove #'null status) " "))
    (put-text-property 0 (length status-str) 'face 'egerrit-status-face status-str)
    status-str))

(defun egerrit--time-to-seconds-str (str)
  "Extract numbers from STR and return number of seconds."
  (pcase-let* ((re-minutes (rx (group (one-or-more digit)) "m"))
               (re-seconds (rx (group (one-or-more digit)) "s"))
               (minutes (if (string-match re-minutes str)
                            (match-string 1 str)
                          "0"))
               (seconds (if (string-match re-seconds str)
                            (match-string 1 str)
                          "0")))
    (+ (* 60 (string-to-number minutes))
       (string-to-number seconds))))

;;;;; Other

(defun egerrit--git-branch-name ()
  "Return the name of the current git branch."
  (string-trim-right
   (shell-command-to-string "git rev-parse --abbrev-ref HEAD")))

(defun egerrit--completing-read-annotator (entity)
  "Create an annotated string of ENTITY."
  (let* ((config (egerrit-annotator-config entity))
         (revision-str (cl-loop for annotation in config
                                collect (let ((str (funcall (plist-get annotation :function) entity)))
                                          (truncate-string-to-width
                                           str
                                           (plist-get annotation :length)
                                           0 ?\s)))))
    `(,(string-join revision-str "") . ,entity)))

(defun egerrit--branch-name (change &optional revision)
  "Return branch name to use for CHANGE optionally with REVISION."
  (let ((author
         (replace-regexp-in-string
          " " "_"
          (downcase
           (egerrit--user-name
            (egerrit--change-owner change)))))
        (id (concat
             (number-to-string (egerrit--change-number change))
             (when-let ((topic (egerrit--change-topic change)))
               (concat "-" topic))
             (when revision
               (format "-patch%s"
                       (egerrit--revision-number revision))))))
    (format "review/%s/%s" author id)))

(defun egerrit--clean-git-repository (change)
  "Check if CHANGE's git repository is clean."
  (let* ((default-directory (egerrit-get-project-root change))
         (git-status
          (string-trim
           (shell-command-to-string "git status --porcelain"))))
    (string-empty-p git-status)))

(defun egerrit--git-switch-command (branch)
  "Return switch command for BRANCH."
  (let ((local-branches (egerrit--git-local-branches)))
    (if (seq-find (lambda (b) (string= b branch)) local-branches)
        ;; Reuse
        (format "git switch %s && git reset --keep FETCH_HEAD" branch)
      ;; Create new
      (format "git switch -c %s" branch))))

(defun egerrit--git-create-branch (branch)
  "Create or reset BRANCH to fetch_head."
  (let ((local-branches (egerrit--git-local-branches)))
    (if (seq-find (lambda (b) (string= b branch)) local-branches)
        ;; Reuse
        (format "git branch --force %s FETCH_HEAD" branch)
      ;; Create new
      (format "git branch %s FETCH_HEAD" branch))))

(defun egerrit--git-local-branches ()
  "Return all local branches."
  (let ((regexp (rx (? "*")
                    (* space)
                    (group (regexp ".*"))))
        (branches (split-string (shell-command-to-string "git branch") "\n" t)))
    (thread-last branches
                 (seq-map (lambda (branch)
                            (string-match regexp branch)
                            (match-string 1 branch))))))

(defun egerrit--map-review-value (review)
  "Return REVIEW value."
  (pcase review
    ('approved "+2")
    ('recommended "+1")
    ('disliked "-1")
    ('rejected "-2")
    (_ "-")))

(defun egerrit--open-revision-diff (revision change)
  "Open the CHANGE's REVISION patch diff."
  (let* ((project-root (egerrit-get-project-root change))
         (default-directory project-root)
         (buffer (egerrit-get-buffer "egerrit-revision-diff"))
         (command
          `("fetch"
            ,(egerrit--download-url (egerrit--fetch-ssh (egerrit--revision-fetch revision)))
            ,(egerrit--download-ref (egerrit--fetch-ssh (egerrit--revision-fetch revision)))))
         (args `("patch-diff" ,buffer "git" ,@command))
         (switch-branch egerrit--switch-branch)
         (mode egerrit--mode))
    (set-process-sentinel
     (apply #'start-process args)
     (lambda (_process event)
       (when (string-match "finished" event)
         (let* ((buffer (egerrit-get-buffer "egerrit-revision-diff"))
                (default-directory project-root)
                (egerrit--current-git-branch (egerrit--git-branch-name))
                (inhibit-read-only t)
                (egerrit--switch-branch switch-branch)
                (egerrit--mode mode))
           (with-current-buffer buffer
             (erase-buffer)
             (setq-local default-directory project-root)
             (process-file-shell-command
              (format "git format-patch --unified=%s -1 --stdout FETCH_HEAD" egerrit-git-diff-context)
              nil t))
           (when egerrit--switch-branch
             (egerrit--checkout-branch change revision))
           (egerrit--prepare-review change revision))
         (deactivate-mark))))))

(defun egerrit--checkout-branch (change revision)
  "Asynchronously checkout CHANGE's REVISION."
  (let* ((branch (egerrit--branch-name change revision))
         (command
          (format "git checkout FETCH_HEAD && %s"
                  (egerrit--git-switch-command
                   branch))))
    (egerrit-request-start-process
     "egerrit-change-checkout"
     command
     :success-message (format "Switched to branch %s" branch))))

(defun egerrit-switch-to-previous-git-branch ()
  "Switch back to previous git branch."
  (when egerrit--current-git-branch
    (let ((command (format "git switch %s" egerrit--current-git-branch)))
      (egerrit-request-start-process
       "egerrit-change-switch"
       command
       :success-message (format "Switched to branch %s" egerrit--current-git-branch)))
    (setq egerrit--current-git-branch nil)))

(defun egerrit--open-partial-revision-diff (old-revision new-revision change)
  "Open a partial diff on CHANGE's OLD-REVISION and NEW-REVISION."
  (let* ((default-directory (egerrit-get-project-root change))
         (old-branch
          (egerrit--branch-name change old-revision))
         (new-branch
          (egerrit--branch-name change new-revision))
         (old-files)
         (commit-message)
         (new-commit-message)
         (old-commit-message)
         (new-files)
         (unique-files)
         (file-hunks)
         (egerrit--current-git-branch (egerrit--git-branch-name)))
    (message "Generating partial diff")

    ;; Download old revision
    (let ((command `("git" nil nil nil "fetch"
                     ,(egerrit--download-url (egerrit--fetch-ssh (egerrit--revision-fetch old-revision)))
                     ,(egerrit--download-ref (egerrit--fetch-ssh (egerrit--revision-fetch old-revision))))))
      (apply #'process-file command))

    ;; Checkout FETCH_HEAD into its own branch
    (process-file-shell-command (egerrit--git-create-branch old-branch))

    ;; List all files modified in the latest commit
    (setq old-files
          (with-temp-buffer
            (apply #'process-file `("git" nil t nil "diff-tree" "--no-commit-id" "--name-only" "-r" "FETCH_HEAD"))
            (split-string
             (buffer-string) "\n" t)))

    ;; Capture commit message from latest commit
    (setq old-commit-message
          (with-temp-buffer
            (apply #'process-file `("git" nil t nil "log" "--pretty=email" "-n" "1" "FETCH_HEAD"))
            (buffer-string)))

    ;; Download new revision
    (let ((command `("git" nil nil nil "fetch"
                     ,(egerrit--download-url (egerrit--fetch-ssh (egerrit--revision-fetch new-revision)))
                     ,(egerrit--download-ref (egerrit--fetch-ssh (egerrit--revision-fetch new-revision))))))
      (apply #'process-file command))

    ;; Checkout FETCH_HEAD into its own branch
    (process-file-shell-command (egerrit--git-create-branch new-branch))

    ;; List all files modified in the latest commit
    (setq new-files
          (with-temp-buffer
            (apply #'process-file `("git" nil t nil "diff-tree" "--no-commit-id" "--name-only" "-r" "FETCH_HEAD"))
            (split-string
             (buffer-string) "\n" t)))

    ;; Capture commit message from latest commit
    (setq new-commit-message
          (with-temp-buffer
            (apply #'process-file `("git" nil t nil "log" "--pretty=email" "-n" "1" "FETCH_HEAD"))
            (buffer-string)))

    ;; Create a unified list of all modified files
    (setq unique-files
          (seq-uniq `(,@new-files ,@old-files)))

    (let ((re-keep-line (rx-to-string `(and (or ,@unique-files))))
          (re-parse-line (rx "R" (one-or-more digit) (one-or-more space) (group (regexp ".*?")) (one-or-more space) (group (regexp ".*?$"))))
          (file-statuses))
      (setq file-statuses
            (with-temp-buffer
              (apply #'process-file `("git" nil t nil "diff" "--name-status" ,old-branch ,new-branch))
              (keep-lines re-keep-line (point-min) (point-max))
              (split-string (buffer-string) "\n" t)))
      (thread-last file-statuses
                   ;; Update renamed files
                   (seq-do (lambda (it)
                             (when (string-match re-parse-line it)
                               (setq unique-files (delete (match-string 1 it) unique-files))
                               (setq unique-files (delete (match-string 2 it) unique-files))
                               (push `(,(match-string 1 it) . ,(match-string 2 it)) unique-files))))))

    ;; Iterate through the list and compare files from both branches
    (setq file-hunks
          (seq-map (lambda (it)
                     (with-temp-buffer
                       (let ((args (if (consp it)
                                       `(,(format "%s:%s" old-branch (car it))
                                         ,(format "%s:%s" new-branch (cdr it)))
                                     `(,old-branch ,new-branch "--" ,it))))
                         (apply #'process-file `("git" nil t nil "diff"
                                                 ,(format "--unified=%s" egerrit-git-diff-context)
                                                 ,@args)))
                       (buffer-string)))
                   unique-files))

    ;; Create a diff of commit messages
    (setq commit-message
          (let ((old (expand-file-name "old_commit_msg" temporary-file-directory))
                (new (expand-file-name "new_commit_msg" temporary-file-directory))
                (commit-diff))
            (with-temp-file old
              (insert old-commit-message))
            (with-temp-file new
              (insert new-commit-message))
            (setq commit-diff
                  (with-temp-buffer
                    (apply #'process-file `("git" nil t nil "diff" "--no-index"
                                            ,(format "--unified=%s" egerrit-git-diff-context)
                                            ,old ,new))
                    (buffer-string)))
            (delete-file old)
            (delete-file new)
            commit-diff))

    ;; Create a buffer and insert commit message followed by file hunks
    (with-current-buffer (egerrit-get-buffer "egerrit-revision-diff")
      (let ((inhibit-read-only t))
        (erase-buffer)
        (insert commit-message)
        (insert "\n")
        (seq-map #'insert file-hunks)))

    (egerrit--switch-branch change new-revision)
    (egerrit--prepare-review change new-revision)))

(defun egerrit--update-git-branch (buffers branch)
  "Update the git BRANCH in BUFFERS."
  (seq-do (lambda (it)
             (with-current-buffer it
               (setq egerrit--current-git-branch branch)))
           buffers))

(defun egerrit--prepare-review (change revision)
  "Prepare for review of CHANGE's REVISION."
  (let ((default-directory (egerrit-get-project-root change))
        (review-buffer (egerrit-get-buffer "egerrit-revision-diff"))
        (conversations-buffer (egerrit-get-buffer "egerrit-conversations"))
        (conversations (egerrit--get-conversations change revision))
        (mode egerrit--mode))
    (switch-to-buffer review-buffer)
    (delete-other-windows)
    (with-current-buffer review-buffer
      (setq-local default-directory (egerrit-get-project-root change))
      (egerrit-diff-mode)
      (setq egerrit--current-change change)
      (setq egerrit--current-revision revision)
      (setq egerrit--conversation-mode 'review)
      (setq egerrit--mode mode)
      (goto-char (point-min))
      (seq-map (lambda (it)
                 (setf (egerrit--conversation-region it)
                       (egerrit--diff-locate-conversation it)))
               conversations))
    (with-current-buffer conversations-buffer
      (setq-local default-directory (egerrit-get-project-root change))
      (egerrit-conversation-mode)
      (egerrit--generate-comments-buffer conversations)
      (setq egerrit--current-change change)
      (setq egerrit--current-revision revision)
      (setq egerrit--conversation-mode 'review)
      (setq egerrit--mode mode)
      (read-only-mode)
      (pop-to-buffer (current-buffer))
      (goto-char (point-min))
      (when-let ((comments (text-property-search-forward 'comments t t)))
        (goto-char (prop-match-beginning comments))
        (recenter))
      (egerrit-focus-on-conversation))
    (when egerrit--switch-branch
      (egerrit--update-git-branch
       `(,review-buffer ,conversations-buffer)
       egerrit--current-git-branch))
    (other-window 1)))

(defun egerrit--sort-revisions (revisions)
  "Sort REVISIONS."
  (seq-sort-by #'egerrit--revision-number #'> revisions))

(defun egerrit--sort-jobs (jobs)
  "Sort JOBS."
  (thread-last (car jobs)
               (seq-sort-by #'egerrit--job-duration #'>)
               (seq-sort-by #'egerrit--job-priority #'>)))

(cl-defgeneric egerrit--job-priority (job)
  "Return the priority number for JOB.")

(cl-defmethod egerrit--job-priority ((job egerrit-job))
  "Return the priority number for JOB."
  (pcase (egerrit-job-status job)
    ("failure" 4)
    ("fast fail" 3)
    ("success" 2)
    ("prev-success" 1)
    (_ 0)))

(cl-defgeneric egerrit--job-duration (job)
  "Return JOB's duration.")

(cl-defmethod egerrit--job-duration ((job egerrit-job))
  "Return JOB's duration."
  (egerrit--time-to-seconds-str
   (egerrit--job-time job)))

(defun egerrit--add-change-jobs (change)
  "Add jobs to CHANGE's revisions."
  (let* ((revisions (egerrit--change-revisions change))
         (messages (egerrit--change-messages change))
         (regexp (funcall egerrit-job-regexp change))
         (jobs (thread-last messages
                            (seq-map (lambda (it) (egerrit--create-jobs it regexp)))
                            (flatten-list))))
    (setf (egerrit--change-revisions change)
          (thread-last revisions
                       (seq-map (lambda (revision)
                                  (setf
                                   (egerrit--revision-jobs revision)
                                   (seq-filter (lambda (job)
                                                 (equal (egerrit--revision-number revision)
                                                        (egerrit--job-revision job)))
                                               jobs))
                                  revision))))
    change))

(defun egerrit--generate-comments-buffer (entities)
  "Generate the comments buffer with ENTITIES."
  (let ((buffer (egerrit-get-buffer "egerrit-conversations")))
    (with-current-buffer buffer
      (let* ((inhibit-read-only t))
        (erase-buffer)
        (insert (egerrit--conversations-str entities))
        (setq egerrit--current-entities entities)))))

(defun egerrit--open-job (job buffer)
  "Open a JOB in BUFFER."
  (with-current-buffer buffer
    (let ((inhibit-read-only t))
      (erase-buffer)
      (insert (egerrit-job-content job))
      (ansi-color-apply-on-region (point-min) (point-max))
      (comint-carriage-motion (point-min) (point-max)))
    (egerrit-job-mode)
    (setq-local next-error-find-buffer-function
                (lambda (_avoid-current
                    _extra-test-inclusive
                    _extra-test-exclusive)
                  buffer))
    (setq egerrit--current-job job)
    (run-hooks 'egerrit-job-hooks)
    (compilation-minor-mode)
    (setq-local font-lock-defaults '(compilation-mode-font-lock-keywords t))
    (read-only-mode)
    (font-lock-mode)
    (pop-to-buffer-same-window buffer)))

(defun egerrit--switch-branch (change revision &optional buffers)
  "Switch to another branch based on CHANGE and REVISION.

Optionally update BUFFERS with current git branch."
  (when egerrit--switch-branch
    (egerrit--update-git-branch
     (or buffers `(,(current-buffer)))
     (egerrit--git-branch-name))
    (egerrit--checkout-branch change revision)))

;;;;; Request

(cl-defun egerrit--request-args (url &key
                                     type
                                     auth
                                     header
                                     params
                                     data
                                     curl-options)
  "Return a list of all arguments parsed to work with `curl'.

This function takes the URL, TYPE, AUTH, HEADER, PARAMS, DATA and
CURL-OPTIONS and conditionally creates a list of the non nil values."
  (seq-remove
   #'null
   (thread-first (egerrit--request-type type)
                 (append curl-options)
                 (append (egerrit--request-auth auth))
                 (append (egerrit--request-header header))
                 (append (egerrit--request-data data))
                 (append (egerrit--request-url url params)))))

(defun egerrit--request-authentication ()
  "Return authentication on the form --user user:pass."
  (let* ((host egerrit-request-url)
         (auth-source-creation-prompts
          `((user . ,(format "%s user: " host))
            (secret . "Password for %u: ")))
         (cred (car (auth-source-search
                     :host host
                     :require '(:user :secret)
                     :create t
                     :max 1))))
    (format "%s:%s"
            (plist-get cred :user)
            (let ((secret (plist-get cred :secret)))
              (if (functionp secret)
                  (funcall secret)
                secret)))))

(defun egerrit--request-type (type)
  "Return a list based on TYPE."
  `("-X" ,type))

(defun egerrit--request-auth (auth)
  "Return a list based on AUTH for curl."
  (when auth
    `("--user"
      ,(egerrit--request-authentication))))

(defun egerrit--request-data (data)
  "Return a list based on DATA for curl."
  (when data
    `("--data" ,(json-encode data))))

(defun egerrit--request-url (url params)
  "Return an url based on URL and PARAMS."
  (if params
      `(,(format "%s?%s"
                 url
                 (egerrit--request-urlencode params)))
    `(,url)))

(defun egerrit--request-urlencode (params)
  "Encode parameters in PARAMS."
  (cl-loop for sep = "" then "&"
           for (k . v) in params
           concat sep
           concat (url-hexify-string (format "%s" k))
           concat "="
           concat (url-hexify-string (format "%s" v))))

(defun egerrit--request-header (header)
  "Convert HEADER to expected by curl."
  (when header
    (cl-loop for (k . v) in header
             collect "--header"
             collect (format "%s: %s" k v))))

;;;; Major mode

(define-derived-mode egerrit-dashboard-mode tabulated-list-mode "Egerrit Dashboard"
  "Mode for `egerrit' dashboard."
  (setq tabulated-list-format
        (cl-loop for elt in egerrit-dashboard-config
                 vconcat `((,(plist-get elt ':name) ,(plist-get elt ':length) ,(plist-get elt ':sort)))))
  (setq-local imenu-create-index-function #'egerrit-dashboard-create-imenu-index)
  (setq tabulated-list-padding 2)
  (setq tabulated-list-sort-key nil)
  (tabulated-list-init-header))

(let ((map egerrit-dashboard-mode-map))
  (define-key map (kbd "a a") #'egerrit-set-assignee)
  (define-key map (kbd "a c") #'egerrit-add-cc-user)
  (define-key map (kbd "a C") #'egerrit-add-cc-group)
  (define-key map (kbd "a d") #'egerrit-download-change)
  (define-key map (kbd "a D") #'egerrit-cherry-pick-change)
  (define-key map (kbd "a h") #'egerrit-add-hashtag)
  (define-key map (kbd "a m") #'egerrit-custom-message)
  (define-key map (kbd "a r") #'egerrit-add-reviewer)
  (define-key map (kbd "a R") #'egerrit-add-review-group)
  (define-key map (kbd "a t") #'egerrit-set-topic)
  (define-key map (kbd "a u") #'egerrit-rebase-change)
  (define-key map (kbd "a w") #'egerrit-change-toggle-wip)
  (define-key map (kbd "a x") #'egerrit-toggle-abandon-change)
  (define-key map (kbd "b") #'egerrit-browse-url)
  (define-key map (kbd "c") #'egerrit-open-conversations)
  (define-key map (kbd "g") #'egerrit-refresh-dashboard)
  (define-key map (kbd "i") #'egerrit-open-info)
  (define-key map (kbd "j") #'egerrit-author-open-job)
  (define-key map (kbd "r j") #'egerrit-open-job)
  (define-key map (kbd "r <return>") #'egerrit-review-change)
  (define-key map (kbd "r S-<return>") #'egerrit-review-partial-change)
  (define-key map (kbd "s") #'imenu)
  (define-key map (kbd "v") #'egerrit-select-dashboard-view)
  (define-key map (kbd "y") #'egerrit-yank-url)
  (define-key map (kbd "<return>") #'egerrit-author-review-change)
  (define-key map (kbd "S-<return>") #'egerrit-author-review-partial-change))

(defun egerrit-get-change-entry (change)
  "Return expected format of CHANGE."
  (if (egerrit-change-p change)
      `(,change
        ,(cl-loop for elt in egerrit-dashboard-config
                  vconcat `(,(funcall (plist-get elt ':function) change))))
    ;; Special handling of headers
    `(,(unless (string-empty-p change) change)
      ,(vconcat
        (progn
          (put-text-property 0 (length change) 'face 'egerrit-heading-face change)
          `[,change])
        (make-vector (1- (length egerrit-dashboard-config)) "")))))

(define-derived-mode egerrit-diff-mode diff-mode "Egerrit Diff"
  "Mode for `egerrit' diff."
  (read-only-mode))

(let ((map egerrit-diff-mode-map))
  (set-keymap-parent map diff-mode-map)
  (define-key map (kbd "C-c '") #'egerrit-create-comment)
  (define-key map (kbd "C-c C-c") #'egerrit-open-conversations-buffer)
  (define-key map (kbd "C-c C-q") #'egerrit-quit-review-or-feedback)
  (define-key map (kbd "C-c C-i") #'egerrit-open-info)
  (define-key map (kbd "C-c C-y") #'egerrit-yank-url)
  (define-key map (kbd "C-c C-r") #'egerrit-set-review-label)
  (define-key map (kbd "C-c C-d") #'egerrit-download-change))

(define-minor-mode egerrit-comment-mode
  "Mode for `egerrit' comment."
  :global nil
  :lighter " Egerrit Comment"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "C-c C-c") #'egerrit-complete-comment)
            (define-key map (kbd "C-c C-k") #'kill-buffer-and-window)
            map))

(define-derived-mode egerrit-conversation-mode fundamental-mode "Egerrit Conversation"
  "Mode for `egerrit' conversations."
  (add-hook 'post-command-hook #'egerrit-focus-on-conversation nil t))

(let ((map egerrit-conversation-mode-map))
  (define-key map (kbd "<return>") #'egerrit-visit-conversation)
  (define-key map (kbd "S-<return>") #'egerrit-visit-conversation-source-code)
  (define-key map (kbd "C-c '") #'egerrit-create-comment)
  (define-key map (kbd "C-c C-k") #'egerrit-kill-comment)
  (define-key map (kbd "C-c C-i") #'egerrit-open-info)
  (define-key map (kbd "C-c C-q") #'egerrit-quit-review-or-feedback)
  (define-key map (kbd "C-c C-y") #'egerrit-yank-url)
  (define-key map (kbd "C-c C-t") #'egerrit-toggle-conversation-status)
  (define-key map (kbd "C-c C-r") #'egerrit-set-review-label)
  (define-key map (kbd "C-c C-d") #'egerrit-download-change)
  (define-key map (kbd "M-n") #'egerrit-next-conversation)
  (define-key map (kbd "M-p") #'egerrit-previous-conversation)
  (define-key map (kbd "C-c C-s") #'egerrit-publish-comments))

(define-derived-mode egerrit-job-mode fundamental-mode "Egerrit Job"
  "Mode for `egerrit' job.")

(let ((map egerrit-job-mode-map))
  (define-key map (kbd "C-c C-d") #'egerrit-download-change)
  (define-key map (kbd "C-c C-y") #'egerrit-yank-url)
  (define-key map (kbd "C-c C-q") #'egerrit-quit-job)
  (define-key map (kbd "C-c C-j") #'egerrit-switch-to-other-job))

(provide 'egerrit)

;;; egerrit.el ends here
