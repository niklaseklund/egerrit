;;; egerrit-test.el --- Tests for egerrit.el -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2021  Niklas Eklund

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Tests for `egerrit'

;;; Code:

;;;; Requirements

(require 'egerrit)
(require 'ert)

;;;; Tests

(ert-deftest egerrit-test-review-str ()
  (let ((change (egerrit--change-create
                 :reviews (list
                           (egerrit--review-create
                            :type 'Code-Review
                            :value 'approved)
                           (egerrit--review-create
                            :type 'Secondary-Review
                            :value 'recommended)
                           (egerrit--review-create
                            :type 'Verified
                            :value 'rejected)))))
    (should (string= "+2" (egerrit--review-str change 'Code-Review)))
    (should (string= "+1" (egerrit--review-str change 'Secondary-Review)))
    (should (string= "-2" (egerrit--review-str change 'Verified)))))

(ert-deftest egerrit-test-revision-jobs-str ()
  ;; Two success One fail
  (let* ((jobs (list (egerrit--job-create :status "success")
                     (egerrit--job-create :status "success")
                     (egerrit--job-create :status "failure")))
         (revision (egerrit--revision-create :jobs jobs)))
    (should (string= "1 - 2" (egerrit--revision-jobs-str revision)))))

;; TODO(Niklas Eklund, 20220324): Enable this test
;; (ert-deftest egerrit-test-updated-str ()
;;   (let ((change (egerrit--change-create :updated  "2021-03-21 15:56:00.12354435")))
;;     (should (string= "Mar 21" (egerrit--updated-str change)))))

(ert-deftest egerrit-test-revision-unresolved-str ()
  (let* ((comments (list (egerrit--comment-create :unresolved t)
                         (egerrit--comment-create :unresolved t)
                         (egerrit--comment-create)
                         (egerrit--comment-create)))
         (revision (egerrit--revision-create :comments comments)))
    (should (equal (egerrit--revision-unresolved-str revision) "2"))))

;; TODO Enable this test
;; (ert-deftest egerrit-test-revision-comment-authors-str ()
;;   (let* ((comments (list (egerrit--comment-create :author (egerrit--user-create :name "Foo"))
;;                          (egerrit--comment-create :author (egerrit--user-create :name "Foo"))
;;                          (egerrit--comment-create :author (egerrit--user-create :name "Bar"))
;;                          (egerrit--comment-create :author (egerrit--user-create :name "Baz"))))
;;          (revision (egerrit--revision-create :comments comments))
;;          (expected "Foo, Bar, Baz"))
;;     (should (equal expected (egerrit--revision-comment-authors-str revision)))))

(ert-deftest egerrit-test-change-status-str ()
  (should (string= "" (egerrit--change-status-str (egerrit--change-create :work-in-progress nil))))
  (should (string= "wip" (egerrit--change-status-str (egerrit--change-create :work-in-progress t))))
  (should (string= "wip" (egerrit--change-status-str (egerrit--change-create :work-in-progress t)))))

(ert-deftest egerrit-test-time-to-seconds-str ()
  (should (= 157 (egerrit--time-to-seconds-str "2m 37s")))
  (should (= 13 (egerrit--time-to-seconds-str "13s"))))

(ert-deftest egerrit-test-sort-revisions ()
  (let* ((revision1 (egerrit--revision-create :number 1))
         (revision2 (egerrit--revision-create :number 2))
         (revision3 (egerrit--revision-create :number 3))
         (revisions `(,revision1 ,revision3 ,revision2)))
    (should (equal `(,revision3 ,revision2 ,revision1)
                   (egerrit--sort-revisions revisions)))))

(ert-deftest egerrit-test-sort-jobs ()
  (let* ((job1 (egerrit--job-create :time "1m 1s" :status "failure"))
         (job2 (egerrit--job-create :time "3m 10s" :status "success"))
         (job3 (egerrit--job-create :time "1m 2s" :status "failure"))
         (jobs `((,job1 ,job3 ,job2))))
    (should (equal `(,job3 ,job1 ,job2)
                   (egerrit--sort-jobs jobs)))))

(ert-deftest egerrit-test-branch-name ()
  ;; Change
  (let ((change1 (egerrit--change-create
                  :number 123
                  :topic "Baz"
                  :owner (egerrit--user-create :name "Foo Bar")))
        (change2 (egerrit--change-create
                  :number 1234
                  :owner (egerrit--user-create :name "Foo Bar"))))
    (should (string= "review/foo_bar/123-Baz" (egerrit--branch-name change1)))
    (should (string= "review/foo_bar/1234" (egerrit--branch-name change2))))

  ;; Revision
  (let ((change (egerrit--change-create
                 :number 123
                 :topic "Baz"
                 :owner (egerrit--user-create :name "Foo Bar")))
        (revision (egerrit--revision-create
                   :number 2)))
    (should (string= "review/foo_bar/123-Baz-patch2" (egerrit--branch-name change revision)))))

(ert-deftest egerrit-test-git-local-branches ()
  (cl-letf* ((output "  branch-name\n  develop\n  drop-git-review\n* master\n")
             ((symbol-function #'shell-command-to-string) (lambda (_input) output))
             (expected '("branch-name" "develop" "drop-git-review" "master")))
    (should (equal expected (egerrit--git-local-branches)))))

(ert-deftest egerrit-test-git-switch-command ()
  (cl-letf* ((local-branches '("develop" "master"))
             ((symbol-function #'egerrit--git-local-branches) (lambda () local-branches)))
    (should (string=  "git switch master && git reset --keep FETCH_HEAD" (egerrit--git-switch-command "master")))
    (should (string=  "git switch -c new"
                      (egerrit--git-switch-command "new")))))


;;;; Libegerrit starts here:


(ert-deftest egerrit-test-create-reviews ()
  (let ((reviews '((Code-Review . ((recommended . ((_account_id . 666)
                                                   (name . "foo")
                                                   (username . "foo_user")
                                                   (email . "")))
                                   (value . 1)))
                   (Verified)
                   (Secondary-Review)))
        (expected (list (egerrit--review-create :type 'Code-Review
                                                   :value 'recommended
                                                   :reviewer (egerrit--user-create
                                                              :name "foo"
                                                              :username "foo_user"
                                                              :email ""))
                        (egerrit--review-create :type 'Secondary-Review
                                                   :reviewer (egerrit--user-create))
                        (egerrit--review-create :type 'Verified
                                                   :reviewer (egerrit--user-create)))))
    (should (equal expected (egerrit--create-reviews reviews)))))

(ert-deftest egerrit-test-create-revisions ()
  (cl-letf* (((symbol-function 'egerrit--create-commit) (lambda (commit) commit))
             ((symbol-function 'egerrit--create-user) (lambda (uploader) uploader))
             (revisions `((abc . ((_number . 1)
                                  (created . "2021")
                                  (uploader . "Foo")
                                  (ref . "abc123")
                                  (commit . "commit")))))
             (expected (list (egerrit--revision-create
                              :id 'abc
                              :number 1
                              :created "2021"
                              :uploader "Foo"
                              :ref "abc123"
                              :fetch (egerrit--create-fetch nil)
                              :commit "commit"))))
    (should (equal expected (egerrit--create-revisions revisions)))))

(ert-deftest egerrit-test-create-range ()
  (let ((range '((start_line . 1)
                 (start_character . 2)
                 (end_line . 10)
                 (end_character . 12)))
        (expected (egerrit--range-create
                   :start-line 1
                   :start-character 2
                   :end-line 10
                   :end-character 12)))
    (should (equal expected (egerrit--create-range range)))))

;;;;; Accessors

(ert-deftest egerrit-test-review ()
  (let* ((reviews
          (list (egerrit--review-create
                 :type 'Code-Review
                 :value 'dissapointed)
                (egerrit--review-create
                 :type 'Secondary-Review
                 :value 'good)
                (egerrit--review-create
                 :type 'Verified)))
         (change (egerrit--change-create :reviews reviews)))
    (should (eq 'dissapointed (egerrit-review change 'Code-Review)))
    (should (eq 'good (egerrit-review change 'Secondary-Review)))
    (should (eq nil (egerrit-review change 'Verified)))))

(ert-deftest egerrit-test-updated ()
  (let* ((time "123456789")
         (change (egerrit--change-create :updated time))
         (job (egerrit--job-create :date time)))
    (should (string= time (egerrit-updated change)))
    (should (string= time (egerrit-updated job)))))

;;;;; Requests

(ert-deftest egerrit-test-set-topic ()
 (cl-letf* ((egerrit-request-url "gerrit.com")
             (change (egerrit--change-create :id "abc123"))
             (topic "foo")
             ((symbol-function 'read-string) (lambda (&rest _) topic))
             ((symbol-function 'egerrit-request)
              (cl-function
               (lambda (url &key _params data auth type _header _parser)
                 (and (string= type "PUT")
                      (equal data `(("topic" . ,topic)))
                      (string= url "https://gerrit.com/a/changes/abc123/topic")
                      auth)))))
    (should (egerrit-set-topic change))))

(ert-deftest egerrit-test-add-reviewer ()
 (cl-letf* ((egerrit-request-url "gerrit.com")
             (change (egerrit--change-create :id "abc123"))
             (reviewer "foo")
             ((symbol-function 'egerrit-request)
              (cl-function
               (lambda (url &key _params data auth type _header _parser)
                 (and (string= type "POST")
                      (equal data `(("reviewer" . ,reviewer)))
                      (string= url "https://gerrit.com/a/changes/abc123/reviewers")
                      auth)))))
    (should (egerrit--post-reviewer reviewer change))))

;;;;; Other

(ert-deftest egerrit-test-project-root ()
  ;; Local project
  (let ((default-directory "/tmp")
        (egerrit-project-roots '(("project1" . "/home/user/project1")
                                    ("project2" . "/home/user/project2"))))
    (should (string= "/home/user/project1" (egerrit-get-project-root (egerrit--change-create :project "project1"))))
    (should (string= "/home/user/project2" (egerrit-get-project-root (egerrit--change-create :project "project2"))))
    (should (string-match "No root found for project:" (egerrit-get-project-root (egerrit--change-create :project "project3")))))
  ;; Remote project
  (cl-letf* ((default-directory "/tmp")
             ((symbol-function 'file-remote-p) (lambda (_path) "/ssh:foo:"))
             (egerrit-project-roots '(("project1" . "/home/user/project1"))))
    (should (string= "/ssh:foo:/home/user/project1" (egerrit-get-project-root (egerrit--change-create :project "project1"))))))

(ert-deftest egerrit-test-read-review-groups ()
  (cl-letf* ((groups '((foo . nil)
                       (bar . nil)
                       (baz . nil)))
             ((symbol-function 'egerrit-request) (lambda (&rest _) groups)))
    (should (equal '("foo" "bar" "baz") (egerrit-review-groups)))))

(ert-deftest egerrit-test-create-jobs ()
  (let* ((message-content
          "Patch Set 7: Verified-1\n\nBuild failed.\n\n- project--job-foo https://cijobs.net/job/foo/71010/ : SUCCESS in 3m 31s\n- project--job-bar https://cijobs.net/job/bar/71010/ : FAILURE in 1m 06s")
         (message (egerrit--message-create :number 1
                                              :message message-content))
         (regexp (rx-to-string
                  `(and bol "- project--"
                        (group (regexp ".*?")) " "
                        (group (regexp ".*?")) " : "
                        (group (regexp ".*?")) " in "
                        (group (regexp ".*"))))))
    (cl-destructuring-bind (job1 job2) (egerrit--create-jobs message regexp)
      ;; Job1
      (should (string= "job-foo" (egerrit--job-name job1)))
      (should (string= "https://cijobs.net/job/foo/71010/" (egerrit--job-url job1)))
      (should (string= "success" (egerrit--job-status job1)))
      (should (string= "3m 31s" (egerrit--job-time job1)))
      ;; Job2
      (should (string= "job-bar" (egerrit--job-name job2)))
      (should (string= "https://cijobs.net/job/bar/71010/" (egerrit--job-url job2)))
      (should (string= "failure" (egerrit--job-status job2)))
      (should (string= "1m 06s" (egerrit--job-time job2))))))

;; TODO(Niklas Eklund, 20220325): Enable this test later
;; (ert-deftest egerrit-test-conversations ()
;;   (let* ((comment1 (egerrit--comment-create
;;                     :id "123"
;;                     :file 'config/foo\.cpp
;;                     :message "This looks wrong"
;;                     :patch-set 1
;;                     :author (egerrit--user-create :name "Foo")))
;;          (comment2 (egerrit--comment-create
;;                     :id "456"
;;                     :in-reply-to "123"
;;                     :message "You are right"
;;                     :file 'config/foo\.cpp
;;                     :patch-set 1
;;                     :author (egerrit--user-create :name "Bar")))
;;          (comments `(,comment1 ,comment2))
;;          (expected (list
;;                     (egerrit--conversation-create :file 'config/foo\.cpp
;;                                                      :id "456"
;;                                                      :patch-set 1
;;                                                      :range (egerrit--comment-range comment1)
;;                                                      :context (egerrit--comment-context comment1)
;;                                                      :side (egerrit--comment-side comment1)
;;                                                      :region '(0 0)
;;                                                      :comments `(,comment1 ,comment2)))))
;;     (should (equal expected (egerrit-get-conversations comments)))))

;;;;; Parsing

(ert-deftest egerrit-request-test-parser ()
  (let ((request-response ")]}'\n[\n  {\n    \"id\": \"foo\",\n    \"project\": \"bar\",\n    \"branch\": \"master\"\n  }\n]\n")
        (expected (vector '((id . "foo")
                            (project . "bar")
                            (branch . "master"))))
        (disabled t))
    ;; TODO: Enable test
    (if disabled
        (should t)
        (should
         (equal expected
                (with-temp-buffer
                  (insert request-response)
                  (egerrit-request-parser)))))))

;;;;; Request

(ert-deftest egerrit-request-test-args ()
  (let ((expected '("-X" "POST" "www.gerrit.com/a/foo"))
        (actual (egerrit--request-args "www.gerrit.com/a/foo"
                                       :type "POST"
                                       :auth nil
                                       :header nil
                                       :params nil
                                       :data nil)))
    (should (equal expected actual))))

(ert-deftest egerrit-request-test-type ()
  (should (equal '("-X" "GET") (egerrit--request-type "GET"))))

(ert-deftest egerrit-request-test-auth ()
  (cl-letf (((symbol-function 'egerrit--request-authentication)
             (lambda () (format "foo:bar"))))
    (should (not (egerrit--request-auth nil)))
    (should (equal '("--user" "foo:bar") (egerrit--request-auth t)))))

(ert-deftest egerrit-request-test-data ()
  (should (equal '("--data" "{\"message\":\"recheck\"}")
                 (egerrit--request-data '(("message" . "recheck"))))))

(ert-deftest egerrit-request-test-url ()
  (should (equal '("https://www.foo.com/a/bar")
                 (egerrit--request-url "https://www.foo.com/a/bar" nil)))
  (should (equal '("https://www.foo.com/a/bar?q=status%3Aopen%20project%3Asrc")
                 (egerrit--request-url "https://www.foo.com/a/bar" `(("q" . "status:open project:src"))))))

(ert-deftest egerrit-request-test-urlencode ()
  (let ((params `(("q" . "status:open project:src")
                  ("n" . 500)
                  ("o" . "LABELS")
                  ("o" . "DETAILED_ACCOUNTS"))))
    (should (string= "q=status%3Aopen%20project%3Asrc&n=500&o=LABELS&o=DETAILED_ACCOUNTS"
                     (egerrit--request-urlencode params)))))

(ert-deftest egerrit-request-test-header ()
  (should (equal '("--header" "Content-Type: application/json")
                 (egerrit--request-header '(("Content-Type" . "application/json")))))
  (should (not (egerrit--request-header nil))))

;;;; Footer

(provide 'egerrit-test)

;;; egerrit-test.el ends here
