;;; guix.scm -- Guix package definition

(use-modules
 (guix packages)
 (guix git-download)
 (guix gexp)
 (guix build-system gnu)
 ((guix licenses) #:prefix license:)
 (guix build-system emacs)
 (gnu packages emacs-xyz)
 (gnu packages screen)
 (ice-9 popen)
 (ice-9 rdelim))

(define %source-dir (dirname (current-filename)))

(define %git-commit
  (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f2" OPEN_READ)))

(define-public emacs-egerrit
  (let ((commit "f5dd7dbf5fa93b8ae5cf7eeb25b994ca5379e41c")
        (revision "0"))
    (package
      (name "emacs-egerrit")
      (version (git-version "0.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/niklaseklund/egerrit")
               (commit commit)))
         (sha256
          (base32
           "0b24izfxyr6r3ch9mrw1gs2ygx20w03y6h9mkf6j9n74zn4sl6yh"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (native-inputs
       `(("emacs-ert-runner" ,emacs-ert-runner)))
      (arguments
       `(#:tests? #t
       #:test-command '("ert-runner")))
      (home-page "https://gitlab.com/niklaseklund/egerrit")
      (synopsis "Emacs to Gerrit user interface")
      (description
       "This package provides Emacs with a user interface to Gerrit. It uses a
REST API to fetch feedback on patches and makes it easy to browse comments as
well as job logs from Emacs.")
      (license license:gpl3+))))

(package
  (inherit emacs-egerrit)
  (name "emacs-egerrit-git")
  (version (git-version (package-version emacs-egerrit) "HEAD" %git-commit))
  (source (local-file %source-dir
                      #:recursive? #t)))
